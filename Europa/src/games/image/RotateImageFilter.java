/**
 * @(#)RotateImageFilter.java
 */
package games.image;

import java.awt.image.ImageFilter;
import java.awt.image.ImageConsumer;
import java.awt.image.ColorModel;
import java.util.Hashtable;

/**
 * RotateImageFilter is an image processing filter extending
 * ImageFilter which rotates an image in 90 degree intervals
 * either clockwise (positive increments) or counterclockwise
 * (negative increments).
 */
public class RotateImageFilter extends ImageFilter {
	int direction;

	/**
	 * Constructs a RotateFilter that rotates in clockwise
	 * 90 degree increments on a positive number and counter-
	 * clockwise 90 degree increments on a negative number.
	 * @param dir the magnitude and direction of the rotatation
	 */
	public RotateImageFilter(int dir) {
		direction = dir;
		direction = direction % 4;
		if (direction < 0)
			direction += 4;
	}

	/**
	 * Pass the properties from the source object along after
	 * adding a property indicating the rotation magnitude and
	 * direction.
	 */
	public void setProperties(Hashtable props) {
		props.put("rotdir", new Integer(direction));
		super.setProperties(props);
	}

	/**
	 * Override the source image's dimensions and pass the dimensions
	 * of the rotated image on to the ImageConsumer.
	 * @see ImageConsumer
	 */
	public void setDimensions(int w, int h) {
		if (direction % 2 != 0) {
			consumer.setDimensions(h, w);
		} else {
			consumer.setDimensions(w, h);
		}
	}

	/**
	 * Converts an image given as an array of bytes to its rotated
	 * form.
	 */
	public void setPixels(int x, int y, int w, int h, ColorModel model,
							byte pixels[], int off, int scansize)
	{
		byte[] data = new byte[w*h];
		int src_index = 0;
		int dst_index = 0;
		switch (direction) {
			case 0:
				consumer.setPixels(x, y, w, h, model, pixels, off, w);
				return;
			case 1:
				for (int r=0; r<h; r++) {
					for (int c=0; c<w; c++) {
						src_index = off + w * r + c;
						dst_index = h * c + (h - r - 1);
						data[dst_index] = pixels[src_index];
					}
				}
				break;
			case 2:
				for (int r=0; r<h; r++) {
					for (int c=0; c<w; c++) {
						src_index = off + w * r + c;
						dst_index = (w * h) - 1 - (w * r + c);
						data[dst_index] = pixels[src_index];
					}
				}
				break;
			case 3:
			default:
				for (int r=0; r<h; r++) {
					for (int c=0; c<w; c++) {
						src_index = off + (w * h) - 1 - (w * r + c);
						dst_index = h * c + (h - r - 1);
						data[dst_index] = pixels[src_index];
					}
				}
				break;
		}
		if (direction % 2 == 0)
			consumer.setPixels(x, y, w, h, model, data, 0, w);
		else
			consumer.setPixels(x, y, h, w, model, data, 0, h);
	}

	/**
	 * This version has not been implemented.
	 */
	public void setPixels(int x, int y, int w, int h, ColorModel model,
							int pixels[], int off, int scansize)
	{
		// not implemented
		System.out.println("IN UNIMPLEMENTED FILTER.");
	}
}
