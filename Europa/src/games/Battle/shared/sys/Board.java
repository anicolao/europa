/*
 * @(#)Board.java
 */
package games.Battle.shared.sys;

/**
 * Board is an abstract base class for the 
 * client and server's representations of the
 * game board. It defines the minimal interface that the board must
 * have in any client or server.
 *
 * @version 1.00
 * @author Jay Steele
 * @author Alex Nicolaou
 */

public abstract class Board
{
	/**
	 * A way to access the single dimensional array as a two dimensional
	 * array when it is convenient to do so.
	 */
	Grid grid;

	/**
	 * Builds a generic board and fills it with cells. The abstract
	 * method makeCell(r,c) is called to actually obtain a cell. This
	 * method must be defined in derived classes to return a particular
	 * (client vs. server) cell type.
	 */
	public Board() {
		grid = new Grid(Rules.rows, Rules.cols);
		for (int r=0; r< Rules.rows; r++) {
			for (int c=0; c< Rules.cols; c++) {
				Cell cell = makeCell(r, c);
				grid.setCell(r, c, cell);
			}
		}
	}

	/**
	 * Return a reference to this board's grid object
	 */
	public Grid getGrid() {
		return grid;
	}

	/**
	 * implemented in derived classes, returns an 
	 * instance of a cell for a particular board class
	 * @param r the row
	 * @param c the column
	 */
	public abstract Cell makeCell(int r, int c);

	/**
	 * return a reference to the cell at the given
	 * row and column
	 * @param r the row
	 * @param c the column
	 */
	public Cell getCell(int r, int c) {
		return (Cell)grid.getCell(r, c);
	}

	/**
	 * clear the modified flag for all cells on the board
	 */
	public void clearModified() {
		for (int r=0; r<Rules.rows; r++) {
			for (int c=0; c<Rules.cols; c++) {
				((Cell)grid.getCell(r,c)).clearModified();
			}
		}
	}

	/**
	 * clear the terrainModified flag for all cells on the board
	 */
	public void clearTerrainModified() {
		for (int r=0; r<Rules.rows; r++) {
			for (int c=0; c<Rules.cols; c++) {
				((Cell)grid.getCell(r,c)).clearTerrainModified();
			}
		}
	}
}
