/*
 * @(#)Symbols.java
 */
package games.Battle.shared.sys;

/**
 * Symbols is a collection of constants that are used throughout the source.
 *
 * @version 1.00
 * @author Jay Steele
 * @author Alex Nicolaou
 */

public class Symbols {
	/**
	 * The maximum number of players allowed in a single game.
	 */
	public static final int MAX_PLAYERS = 5;
	/**
	 * The number of terrain levels
	 */
	public static final int NUM_TERRAIN_LEVELS = 8;
	/**
	 * The most troops that can be in a cell on the server side. The server
	 * stores the number of troops in a cell at a greater precision than the
	 * client to help keep transmission costs down.
	 */
	public static final int MAX_SERVER_TROOPS = 100;
	/**
	 * The most troops that can be in a cell on the client side.
	 */
	public static final int MAX_CLIENT_TROOPS = 31;
	/**
	 * The width of the board in pixels
	 */
	public static final int BOARD_W = 512;
	/**
	 * The height of the board in pixels
	 */
	public static final int BOARD_H = 512;

	/**
	 * NORTH, the north direction for a pipe
	 */
	public static final int NORTH 	= 0;
	/**
	 * EAST, the east direction for a pipe
	 */
	public static final int	EAST 	= 1;
	/**
	 * SOUTH, the south direction for a pipe
	 */
	public static final int	SOUTH 	= 2;
	/**
	 * WEST, the west direction for a pipe
	 */
	public static final int	WEST 	= 3;

	/**
	 * NORTHEAST indicates both a north and an east pipe
	 */
	public static final int NORTHEAST 	= 4;
	/**
	 * SOUTHEAST indicates both a south and an east pipe
	 */
	public static final int SOUTHEAST 	= 5;
	/**
	 * SOUTHWEST indicates both a south and a west pipe
	 */
	public static final int SOUTHWEST	= 6;
	/**
	 * NORTHWEST indicates both a north and a west pipe
	 */
	public static final int NORTHWEST	= 7;

	/**
	 * a mask for extracting pipes from a packed byte
	 */
	public static final int[] PIPE_MASK = { 0x01, 0x02, 0x04, 0x08 };

	/**
	 * occupancy defines for the various players
	 */
	public static final int PLAYER0 		= 0x0000;
	public static final int PLAYER1 		= 0x0001;
	public static final int PLAYER2 		= 0x0002;
	public static final int PLAYER3 		= 0x0003;
	public static final int PLAYER4 		= 0x0004;
	/**
	 * an array of player symbols for the various players
	 */
	public static final int PLAYER_SYMBOL[] = { 
		Symbols.PLAYER0, 
		Symbols.PLAYER1, 
		Symbols.PLAYER2,
		Symbols.PLAYER3,
		Symbols.PLAYER4,
		Symbols.UNOCCUPIED // used for observers
	};
	/**
	 * the value of an unoccupied cell
	 */
	public static final int UNOCCUPIED 		= 0x0005;
	/**
	 * the value of an invisible cell
	 */
	public static final int INVISIBLE 		= 0x0006;
	/**
	 * the value of an unmodified cell
	 */
	public static final int UNMODIFIED 		= 0x0007;

	/**
	 * strings used for debugging printouts, arranged so that
	 * the index matches the occupancy value
	 */
	public static String[] occupancyString = {
		"Player0",
		"Player1",
		"Player2",
		"Player3",
		"Player4",

		"Unoccupied",
		"Invisible",
		"Unmodified"
	};

	/**
	 * command to connect a pipe, sent from client to server.
	 */
	public static final short NORTH_PIPE 	= (short)0x0000;
	/**
	 * command to connect a pipe, sent from client to server.
	 */
	public static final short EAST_PIPE 	= (short)0x0001;
	/**
	 * command to connect a pipe, sent from client to server.
	 */
	public static final short SOUTH_PIPE	= (short)0x0002;
	/**
	 * command to connect a pipe, sent from client to server.
	 */
	public static final short WEST_PIPE 	= (short)0x0003;
	/**
	 * command to connect a pipe, sent from client to server.
	 */
	public static final short X_NORTH_PIPE 	= (short)0x0004;
	/**
	 * command to connect a pipe, sent from client to server.
	 */
	public static final short X_EAST_PIPE 	= (short)0x0005;
	/**
	 * command to connect a pipe, sent from client to server.
	 */
	public static final short X_SOUTH_PIPE	= (short)0x0006;
	/**
	 * command to connect a pipe, sent from client to server.
	 */
	public static final short X_WEST_PIPE 	= (short)0x0007;
	/**
	 * command to connect a pipe, sent from client to server.
	 */
	public static final short CLEAR_PIPES	= (short)0x0008;
	/**
	 * command to build a city (not implemented)
	 */
	public static final short BUILD 		= (short)0x0009;
	/**
	 * command to scuttle a city (not implemented)
	 */
	public static final short SCUTTLE 		= (short)0x000a;
	/**
	 * command to dig land (not implemented)
	 */
	public static final short DIG 			= (short)0x000b;
	/**
	 * command to fill land (not implemented)
	 */
	public static final short FILL 			= (short)0x000c;
	/**
	 * command to paratroop into a square
	 */
	public static final short PARATROOP 	= (short)0x000d;
	/**
	 * command to gun a square
	 */
	public static final short GUN			= (short)0x000e;
	/**
	 * command to set reserves (not used yet)
	 */
	public static final short RESERVES 		= (short)0x000f;
	/**
	 * command to surrender
	 */
	public static final short SURRENDER 	= (short)0x0010;
	/**
	 * command to signal an error, sent from client to server
	 */
	public static final short ERROR			= (short)0xffff;

	/**
	 * strings for debugging client->server messages, 
	 * arranged so that index matches value
	 */
	public static String[] commandString = {
		"North_Pipe",
		"East_Pipe",
		"South_Pipe",
		"West_Pipe",
		"Build",
		"Scuttle",
		"Dig",
		"Fill",
		"Gun",
		"Paratroop",
		"Reserves",
		"Surrender",
		"Error"
	};

	/**
	 * indicate a game init packet by the server (not yet used)
	 */
	public static final short GAME_INIT			= (short)0x0000;
	/**
	 * indicate a terrain init packet by the server (not yet used)
	 */
	public static final short TERRAIN_INIT		= (short)0x0001;
	/**
	 * indicate a board diff packet by the server (not yet used)
	 */
	public static final short BOARD_DIFF		= (short)0x0002;
	/**
	 * indicate a player surrender packet by the server (not yet used)
	 */
	public static final short PLAYER_SURRENDER	= (short)0x0003;

	/**
	 * for debugging server messages, indexes arranged to match value
	 * of constants.
	 */
	public static String[] tagString = {
		"Game_Init",
		"Terrain_Init",
		"Board_Diff",
		"Player_Surrender"
	};
}
