/*
 * @(#)Common.java
 */
package games.Battle.shared.sys;

import java.util.Random;
import java.lang.Math;

/**
 * Common is a collection of static functions that perform a packing and
 * unpacking services for both the client and server side.
 *
 * @version 1.00
 * @author Jay Steele
 * @author Alex Nicolaou
 */

public class Common {

	/**
	 * a random number generator.
	 */
	private static Random randGen = new Random();

	/**
	 * return whether the number representing a cell
	 * occupancy is a player (not invisible, not
	 * unoccupied and not unchanged)
	 * @param occupancy a particular cell's occupancy
	 */
	public static final boolean isPlayer(int occupancy) {
		return (occupancy >= Symbols.PLAYER0)
				&& (occupancy <= Symbols.PLAYER4);
	}

	/**
	 * extract the occupancy from a packed byte
	 */
	public static final int extractOccupancy(byte b) { 
		return (b >> 5) & 0x0007;
	}
	/**
	 * extract the terrain fom a packed byte
	 */
	public static final int extractTerrain(byte b) { 
		return (b >> 2) & 0x0007; 
	}
	/**
	 * extract the number of troops from a packed byte
	 */
	public static final int extractTroops(byte b) { 
		return b & 0x001f; 
	}
	/**
	 * extract the pipes froma packed byte.
	 */
	public static final int extractPipes(byte b) { 
		return (b >> 4) & 0x000f; 
	}
	/**
	 * extract the run length encoding count from a packed byte.
	 */
	public static final int extractCount(byte b) { 
		return b & 0x000f; 
	}

	/**
	 * stuffs occupancy in bits 7-5, and terrain in bits 4-2
	 */
	public static final byte stuffOccupancyAndTerrain(int o, int t) {
		int a = (o << 5) & 0x00e0;
		int b = (t << 2) & 0x001c;
		return (byte)(a | b);
	}

	/**
	 * stuffs occupancy and a 4-bit counter int a byte
	 */
	public static final byte stuffOccupancyAndCount(int o, int c) {
		return (byte)((o << 5) | (c & 0x000f));
	}

	/**
	 * stuffs occupancy and troops
	 */
	public static final byte stuffOccupancyAndTroops(int o, int t) {
		return (byte)((o << 5) | (t & 0x001f));
	}

	/**
	 * stuffs pipes into hi byte
	 */
	public static final byte stuffPipes(int p) {
		return (byte)(p << 4);
	}

	/**
	 * converts a (row,col) tuple to a unique index in a contiguous
	 * array
	 */
	public static final int RCtoIndex(int r, int c) {
		return (r*Rules.cols+c);
	}

	/**
	 * converts an index to a row
	 */
	public static final int indexToRow(int index) {
		return index / Rules.rows;
	}
	/**
	 * converts an index to a column
	 */
	public static final int indexToCol(int index) {
		return index % Rules.rows;
	}

	/**
	 * returns a random row 
	 */
	public static final int getRandomRow() {
		return Math.abs(randGen.nextInt()) % Rules.rows;
	}
	/**
	 * returns a random column
	 */
	public static final int getRandomCol() {
		return Math.abs(randGen.nextInt()) % Rules.cols;
	}

	/**
	 * wraps System.exit() to be friendly as a standlaone application
	 * and as an applet.
	 */
	 public static final void exit(int code) {
		try {
			System.exit(code);
		} catch ( Exception e ) {
			// ignore any problems
		}
	 }
}
