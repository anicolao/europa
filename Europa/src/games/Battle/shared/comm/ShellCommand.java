/*
 * @(#)ShellCommand.java
 */
package games.Battle.shared.comm;

/**
 * ShellCommand is a collection of tags for messages passed between the client
 * shell and the server core. This packets preceeds other packets which contain
 * the real data of the command, and serve as a type tag that indicates what
 * data will follow.
 *
 * @version 1.00
 * @author Alex Nicolaou
 * @author Jay Steele
 */

public class ShellCommand extends BattlePacket {
	/**
	 * JOIN joins a game
	 */
	public static byte JOIN  = 0;
	/**
	 * QUIT quits a game
	 */
	public static byte QUIT  = 1;
	/**
	 * WHO preceeds a list of WhoInfo packets.
	 */
	public static byte WHO   = 2;
	/**
	 * GAMES proceeds a list of GameInfo packets.
	 */
	public static byte GAMES = 3;
	/**
	 * SHUTDOWN tells the server to shut down.
	 */
	public static byte SHUTDOWN   = 4;
	/**
	 * DISCONNECT quits the connection with the server.
	 */
	public static byte DISCONNECT = 5;
	/**
	 * UPDATEID lets the player change their preferences, info, and password.
	 */
	public static byte UPDATEID   = 6;
	/**
	 * CHATMSG preceeds an InfoPacket with the text of the player's choice.
	 */
	public static byte CHATMSG    = 7;

	/**
	 * bytes for transmitting the command
	 */
	byte[] cmd = new byte[2];

	/**
	 * Construct an empty ShellCommand to be read from an input stream.
	 */
	public ShellCommand() {
		cmd[0] = 0;
		cmd[1] = 0;
	}

	/**
	 * Construct a complete shell command.
	 */
	public ShellCommand(byte command, int gameid) {
		cmd[0] = command;
		cmd[1] = (byte)gameid;
	}

	/**
	 * Return the command code, which must be one of JOIN, QUIT, etc.
	 */
	public byte getCommand() { 
		return cmd[0];
	}

	/**
	 * Return the gameId that this command applies to. 0 in the case of
	 * a command that isn't associated with a particular game.
	 */
	public int getGameId() { 
		return ((int)cmd[1])&0x00ff;
	}

	/**
	 * Return the byte representation.
	 */
	protected byte[] toBytes() {
		return cmd;
	}

	/**
	 * With the given array of bytes, fill this command object
	 * with data.
	 * @param data the data to fill the command with.
	 */
	protected void fromBytes(byte[] data) {
		// probably not worth the overhead of memcpy() since native calls 
		// are probably costlier than just doing the assignments.
		cmd[0] = data[0];
		cmd[1] = data[1];
	}
}
