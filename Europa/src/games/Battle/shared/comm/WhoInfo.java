/*
 * @(#)WhoInfo.java
 */

package games.Battle.shared.comm;

import games.Rating.Rating;

import java.io.*;
import java.util.*;

/**
 * WhoInfo is a specialized PlayerInfo packet that avoids setting the 
 * password. It provides a convenient way to tell each player about other
 * players without compromising the security of the passwords. 
 *
 * @version 1.00
 * @author  Alex Nicolaou
 * @author  Jay Steele
 */

public class WhoInfo extends PlayerInfo
{
	/**
	 * Constructs a WhoInfo packet from a PlayerInfo packet, stripping 
	 * the password in the process.
	 * @param p is the player info packet.
	 */
	public WhoInfo(PlayerInfo p) {
		this.handle = p.handle; 
		this.name = p.name;
		this.mail = p.mail;
		this.pass = new String("");
		this.wizard = p.wizard;
		this.rating = p.rating;
	}

	/**
	 * Constructs an empty packet  ready to be read in from an inputstream.
	 */
	public WhoInfo() {
	}

	/**
	 * Resets the info in the who packet based on the PlayerInfo packet.
	 */
	public void setInfo(PlayerInfo p) {
		this.handle = p.handle; 
		this.name = p.name;
		this.mail = p.mail;
		this.pass = new String("");
		this.wizard = p.wizard;
		this.rating = p.rating;
	}

	/**
	 * Return a string suitable for display to the end user.
	 */
	public String toString() {
		String s;
		s = handle;
		if (wizard)
			s = s + " !\t";
		else
			s = s + "  \t";
		if (handle.length() < 7)
			s = s + "\t";
		s = s + rating + "\t" + name;
		if (mail.length() > 1)
			s = s + " (" + mail + ")";
		return s;
	}
}
