/*
 * @(#)GameInit.java
 */

package games.Battle.shared.comm;

import games.Battle.shared.sys.*;
import java.awt.Color;

/**
 * A GameInit packet is the information which is sent from the
 * server to each client at the beginning of a game in order to
 * set up preamble information like the Rules of the game, the
 * colors of each of the players.
 * 
 * @version 1.00 02/02/96
 * @author Jay Steele
 */
public class GameInit extends BattlePacket {
	/**
	 * the player number that this game init packet is for.
	 */
	int player;

	/**
	 * the array of colours used for this players in this game.
	 */
	Color[] colors;

	/** 
	 * the default colours used by games.
	 */
	public static final Color[] DEFAULT_COLORS = {
		new Color(255, 0, 0),
		new Color(0, 255, 0),
		new Color(0, 0, 255),
		new Color(255, 0, 255),
		new Color(255, 255, 0)
	};

	/**
	 * The default constructor which fills the GameInit packet with
	 * default data. This contructor is used by the client application
	 * prior to calling the readFrom(is) method to obtain a game init
	 * packet from a data stream.
	 */
	public GameInit() {
		this.player = 0;
		this.colors = new Color[Symbols.MAX_PLAYERS];
	}

	/**
	 * The data constructor for the GameInit which fills it with
	 * the given data. This contructor is used by the server application
	 * prior to calling the writeTo(os) method to send a game init
	 * packet to a data stream.
	 * @param player the player identifier
	 * @param an array of colors representing the color for each player
	 */
	public GameInit(int player, Color[] colors) {
		this.player = player;
		this.colors = colors;
	}

	/**
	 * Return the player identifier of this game initializer
	 */
	public int getPlayer() { return player; }

	/**
	 * Produce an array of bytes representing a GameInit
	 */
	public byte[] toBytes() {
		byte[] buffer = new byte[1 + 3 * (Symbols.MAX_PLAYERS)];
		buffer[0] = (byte)player;
		for (int i=0; i<Symbols.MAX_PLAYERS; i++) {
			buffer[3*i+1] = (byte)colors[i].getRed();
			buffer[3*i+2] = (byte)colors[i].getGreen();
			buffer[3*i+3] = (byte)colors[i].getBlue();
		}
		return buffer;
	}

	/**
	 * Fill this GameInit packet with the given array of bytes.
	 * @param buffer the byte array from which to fill this GameInit
	 */
	public void fromBytes(byte[] buffer) {
		player = buffer[0];
		for (int i=0; i<Symbols.MAX_PLAYERS; i++) {
			int r = (int)buffer[3*i+1];
			int g = (int)buffer[3*i+2];
			int b = (int)buffer[3*i+3];
			if (r < 0) r = 255;
			if (g < 0) g = 255;
			if (b < 0) b = 255;
			colors[i] = new Color(r, g, b);
		}
	}
}
