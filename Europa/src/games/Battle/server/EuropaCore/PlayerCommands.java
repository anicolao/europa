/*
 * @(#)PlayerCommands.java
 */
package games.Battle.server.EuropaCore;

import java.io.*;
import java.net.*;
import java.util.*;

import games.Battle.shared.comm.*;
import games.Battle.shared.sys.*;

/**
 * PlayerCommands watches the input of a particular player and carries
 * out that player's requests to join or quit games, or to chat, or whatever
 * else the player wants to do.
 *
 * @version 1.00
 * @author Alex Nicolaou
 * @author Jay Steele
 */

class PlayerCommands implements Runnable {
	/** 
	 * the thread for listening to this socket
	 */
	Thread playerThread;

	/**
	 * the pointer back to the main server
	 */
	EuropaCore server;
	/**
	 * the input stream of commands from the player
	 */
	InputStream in;
	/**
	 * the id of this player
	 */
	PlayerInfo id;

	/**
	 * constructs the player commands thread.
	 * @param server the main server
	 * @param in the player's input stream
	 * @param p the player's id record
	 */
	public PlayerCommands(EuropaCore server, InputStream in, PlayerInfo p) {
		this.server = server;
		this.in = in;
		id = p;
	}

	/**
	 * start the thread running and listening to the player.
	 */
	public void start() {
		if (playerThread != null)
			return;

		playerThread = new Thread(this, "Main Player Input");
		playerThread.start();
	}

	/**
	 * stop listening to this player.
	 */
	public void stop() {
		playerThread.stop();
	}

	/**
	 * loop forever, processing the player's requests. Stop if the user
	 * quits.
	 */
	public void run() {
		try {
			while (true) {
				ShellCommand cmd = new ShellCommand();
				cmd.readFrom(in);
				if (cmd.getCommand() == ShellCommand.JOIN) {
					server.joinGame(cmd.getGameId(), id);
				}
				else if (cmd.getCommand() == ShellCommand.QUIT) {
					server.quitGame(cmd.getGameId(), id);
				}
				else if (cmd.getCommand() == ShellCommand.DISCONNECT) {
					server.logoutPlayer(id);
					stop();
				}
				else if (cmd.getCommand() == ShellCommand.SHUTDOWN) {
					server.shutdown(id);
				}
				else if (cmd.getCommand() == ShellCommand.UPDATEID) {
					PlayerInfo newId = new PlayerInfo();
					newId.readFrom(in);
					server.updatePlayer(id, newId);
				}
				else if (cmd.getCommand() == ShellCommand.CHATMSG) {
					InfoPacket msg = new InfoPacket();
					msg.readFrom(in);
					server.chatMessage(id, msg.toString());
				}
			}
		}
		catch (Exception e) {
			// quietly lose the connection.
		}
	}
}
