/*
 * @(#)RejectPlayer.java
 */
package games.Battle.server.EuropaCore;

import java.io.*;
import java.net.*;
import java.util.*;

import games.Battle.shared.comm.*;

/** 
 * A thread to kick a player off the server when the server
 * is totally full.
 *
 * @version 1.00
 * @author Alex Nicolaou
 * @author Jay Steele
 */

class RejectPlayer implements Runnable {
	/**
	 * a thread to reject the player
	 */
	Thread playerThread;

	/**
	 * a socket to the player
	 */
	Socket player;

	/**
	 * construct the object to reject the player
	 * @param player the socket to kill
	 */
	public RejectPlayer(Socket player) {
		this.player = player;
	}

	/**
	 * start the thread
	 */
	public void start() {
		if (playerThread != null)
			return;

		playerThread = new Thread(this, "Maytag Thread");
		playerThread.start();
	}

	/**
	 * stop the thread
	 */
	public void stop() {
		playerThread.stop();
	}

	/**
	 * talk to the client to tell the player to go away.
	 */
	public void run() {
		try {
			InfoPacket rp;
			rp = new InfoPacket("Couldn't log in: game is full. Try later.", 
								InfoPacket.FatalError);
			
			OutputStream out = player.getOutputStream();
			rp.writeTo(out);
			out.flush();
			Thread.sleep(1000);
			player.close();
		}
		catch (Exception e) {
		}

		return;
	}
}
