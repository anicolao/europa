/*
 * @(#)PlayerConnection.java
 */
package games.Battle.server.EuropaCore;

import java.io.*;
import java.net.*;
import java.util.*;

import games.Battle.shared.comm.*;
import games.Battle.shared.sys.*;

/**
 * PlayerConnection manages the player's actual logging in negotiations, and
 * then manages all further output from the server core to the player, like
 * keeping the who, chat, and game info up to date by transmitting it whenever
 * it changes.
 *
 * @version 1.00
 * @author Alex Nicolaou
 * @author Jay Steele
 */

class PlayerConnection implements Runnable {
	/**
	 * the thread for this connection
	 */
	Thread playerThread;

	/**
	 * a reference back to the main server
	 */
	EuropaCore server;
	/**
	 * the socket that this player is connected on 
	 */
	Socket player;
	/**
	 * the id record for this player
	 */
	PlayerInfo id;

	/**
	 * the last game up date that we sent to this player. If the current
	 * update is more recent, we need to retransmit.
	 */
	int lastGameUpdate = 0;
	/**
	 * the last who update that we sent to this player. If the current
	 * who update is more recent, we need to retransmit the who list.
	 */
	int lastWhoUpdate = 0;
	/**
	 * the last chat message we sent. if the current message id is more
	 * recent we need to retransmit all the intervening messages that are
	 * still in the server's history.
	 */
	int lastChatUpdate = -1;

	/**
	 * construct a new connection
	 * @param server the server who connected this player
	 * @param player the socket to the new player connection
	 */
	public PlayerConnection(EuropaCore server, Socket player) {
		this.server = server;
		this.player = player;
		lastChatUpdate = server.lastChatUpdate();
	}

	/**
	 * start the player connection thread
	 */
	public void start() {
		if (playerThread != null)
			return;

		playerThread = new Thread(this, "Main Player Connection");
		playerThread.start();

	}

	/**
	 * stop the player connection thread
	 */
	public void stop() {
		playerThread.stop();
	}

	/**
	 * manage log-in, spawn an input task (PlayerCommand task), and then loop
	 * forever updating the who list, game info, and chat info.
	 */
	public void run() {
		try {
			InputStream in = player.getInputStream();
			OutputStream out = player.getOutputStream();

			String banner = "\r\n\r\nEuropa Game Server Core. Version 1.0\r\n";
			banner = banner + "If you are reading this text you are not\r\n";
			banner = banner + "using a client. Disconnect and try again.\r\n";
			InfoPacket bannerPacket;
			bannerPacket = new InfoPacket(banner, InfoPacket.Info);
			bannerPacket.writeTo(out);

			int loginFailed = 0;
			do {
				PlayerInfo loginid = new PlayerInfo();
				loginid.readFrom(in);

				id = server.authorizePlayer(loginid);
				if (id == null && server.newPlayer(loginid)) {
					InfoPacket rp;
					rp = new InfoPacket("Couldn't log in: login or password incorrect.", 
										InfoPacket.FatalError);
					rp.writeTo(out);
					loginFailed++;
				}
				else loginFailed = 0;

				if (id == null)
					id = server.authorizePlayer(loginid);
			} while (loginFailed > 0 && loginFailed < 3);

			if (loginFailed > 0) {
				player.close();
				return;
			}

			InfoPacket rp;
			rp = new InfoPacket("Login Succeeded.", InfoPacket.Info);
			rp.writeTo(out);

			// send the user total info about the account
			id.writeTo(out);

			PlayerCommands input = new PlayerCommands(server, in, id);
			input.start();

            // send motd
            Vector  motd = server.getMOTD();
            if (motd.size() > 0) {
                ShellCommand type;
                type = new ShellCommand(ShellCommand.CHATMSG, 0);
                type.writeTo(out);

                CountPacket c= new CountPacket(motd.size());
                c.writeTo(out);

                Enumeration e = motd.elements();
                while (e.hasMoreElements()) {
                    String m = (String)e.nextElement();
                    InfoPacket i=new InfoPacket(m, InfoPacket.Info);
                    i.writeTo(out);
                }
            }

			while (true) {
				try {
					Thread.sleep(200);

					if (server.lastGameUpdate() != lastGameUpdate) {
						lastGameUpdate = server.lastGameUpdate();
						if (server.games.length > 0) {
							ShellCommand type;
							type = new ShellCommand(ShellCommand.GAMES, 0);
							type.writeTo(out);

							CountPacket c= new CountPacket(server.games.length);
							c.writeTo(out);

							for (int i = 0; i < server.games.length; i++) {
								BattlePacket game = server.games[i];
								game.writeTo(out);
							}
						}
					}
					if (server.lastWhoUpdate() != lastWhoUpdate) {
						lastWhoUpdate = server.lastWhoUpdate();

						ShellCommand type;
						type = new ShellCommand(ShellCommand.WHO, 0);
						type.writeTo(out);

						// clone the who list so that if someone logs in
						// while we're transmitting we don't mess up
						Vector who = (Vector)server.who.clone();
						CountPacket c= new CountPacket(who.size());
						c.writeTo(out);

						Enumeration e = who.elements();
						WhoInfo whoRecord = new WhoInfo();
						while (e.hasMoreElements()) {
							PlayerInfo p = (PlayerInfo)e.nextElement();
							whoRecord.setInfo(p);
							whoRecord.writeTo(out);
						}
					}
					if (server.lastChatUpdate() != lastChatUpdate) {
						int updateLimit = server.lastChatUpdate();

						Vector messages = new Vector();
						for (;lastChatUpdate < updateLimit; lastChatUpdate++) {
							String s = server.getChatMessage(lastChatUpdate);
							if (s != null)
								messages.addElement(s);
						}

						if (messages.size() > 0) {
							ShellCommand type;
							type = new ShellCommand(ShellCommand.CHATMSG, 0);
							type.writeTo(out);

							CountPacket c= new CountPacket(messages.size());
							c.writeTo(out);

							Enumeration e = messages.elements();
							while (e.hasMoreElements()) {
								String m = (String)e.nextElement();
								InfoPacket i=new InfoPacket(m, InfoPacket.Info);
								i.writeTo(out);
							}
						}
					}
				}
				catch (Exception e) {
					// player must have linked out, log him off
					server.logoutPlayer(id);
					return;
				}
			}
		}
		catch (Exception e) {
			// quietly lose the connection.
		}
	}
}
