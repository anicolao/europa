/*
 * @(#)DummyRef.java
 */
package games.Battle.server.BattleGame;

/**
 * A referee that does nothing so that the game thread can be run outside
 * the complete server.
 */

import games.Battle.server.ServerBoard.GameReferee;

public class DummyRef implements GameReferee {
	public DummyRef() {}
	public void playerDied(int playerNum) {}
	public void gameOver() {}
}
