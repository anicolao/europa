/*
 * @(#)GeoTile.java
 */
package games.Battle.server.GeoMorph;

import java.io.*;
import java.util.*;

/**
 * GeoTile implements one particular tile in a geomorph world.
 *
 * @version 1.00
 * @author Alex Nicolaou
 * @author Jay Steele
 */

public class GeoTile implements Cloneable {
	/**
	 * The number of squares in a particular geomorph tile
	 */
	static final public int GEO_TILESIZE = 4;

	/**
	 * direction constant: north
	 */
	static final public int NORTH = 0;
	/**
	 * direction constant: east
	 */
	static final public int EAST = 1;
	/**
	 * direction constant: south
	 */
	static final public int SOUTH = 2;
	/**
	 * direction constant: west
	 */
	static final public int WEST = 3;

	/**
	 * a unique number for this tile
	 */
	int tileID;
	/**
	 * the actual terrain in this tile
	 */
	public int[][] tile;
	/**
	 * the location of the city hint for this tile (place a city here, 
	 * if anywhere!)
	 */
	public int cityX, cityY;
	/**
	 * a flag to indicate if there is a city in this tile
	 */
	boolean hasCity = false;

	/**
	 * an array of vectors, one for each direction. Each vector
	 * contains a list of valid tile neighbours in that direction.
	 * validity is determined by comparing the continuity of the
	 * terrain along that edge of the tile to avoid introducing 
	 * badly mismatched terrain.
	 */
	public Vector[] validTiles; 

	/**
	 * construct a tile be reading it in from the data input stream.
	 * the id given to the tile is assumed to be unique.
	 * @param data an input stream that contains a tile data file
	 * @param id an id that is assumed to be unique to this tile
	 */
	public GeoTile(DataInput data, int id) throws java.io.IOException {
		tileID = id;

		validTiles = new Vector[4];
		for (int dir = 0; dir < 4; dir++) {
			validTiles[dir] = new Vector();
		}

		tile = new int[GEO_TILESIZE][GEO_TILESIZE];

		for (int i = 0; i < GEO_TILESIZE; i++) {
			for (int j = 0; j < GEO_TILESIZE; j++) {
				tile[i][j] = (int)data.readByte();
			}
		}

		cityX = (int)data.readByte();
		cityY = (int)data.readByte();

		if (tile[cityX][cityY] == 0) {
			tile[cityX][cityY] = 1;
		}
	}

	/**
	 * rotate this tile 90 degrees to create a "new" tile
	 */
	public void rotate90() {
		// shift the terrain around by 90 degrees
		int newTerrain[][] = new int[GEO_TILESIZE][GEO_TILESIZE];

		for (int x = 0; x < GEO_TILESIZE; x++) {
			for (int y = 0; y < GEO_TILESIZE; y++) {
				newTerrain[x][y] = tile[(GEO_TILESIZE - 1 - y)][x];
			}
		}

		for (int x = 0; x < GEO_TILESIZE; x++) {
			for (int y = 0; y < GEO_TILESIZE; y++) {
				tile[x][y] = newTerrain[x][y];
			}
		}

		int tmp = cityX;
		cityX = GEO_TILESIZE - 1 - cityY;
		cityY = tmp;
	}

	/**
	 * return an exact copy of this tile
	 */
	public Object clone() {
		try {
			GeoTile newtile = (GeoTile)super.clone();

			newtile.tileID = tileID;
			newtile.tile = new int[GEO_TILESIZE][GEO_TILESIZE];

			// Q: can't find a System.arraycopy for 2d arrays 
			for (int i = 0; i < GEO_TILESIZE; i++) {
				for (int j = 0; j < GEO_TILESIZE; j++) {
					newtile.tile[i][j] = tile[i][j];
				}
			}

			newtile.cityX = cityX;
			newtile.cityY = cityY;
			newtile.hasCity = hasCity;

			newtile.validTiles = new Vector[4];

			for (int d = 0; d < 4; d++) {
				newtile.validTiles[d] = (Vector)validTiles[d].clone();
			}

			return  newtile;
		}
		catch (Exception e) {
			throw new InternalError();
		}
	}

	/**
	 * compare us to the other tile and add the other tile to our legal
	 * neighbour lists in each direction
	 */
	public boolean checkCompatibility(GeoTile other) {
		int DIFF = 3;

		boolean north = true;
		boolean east = true;
		boolean south = true;
		boolean west = true;

		for (int x = 0; x < GEO_TILESIZE; x++) {
			if (Math.abs(tile[x][0] - other.tile[x][GEO_TILESIZE - 1]) > DIFF)
				north = false;
			if (Math.abs(other.tile[x][0] - tile[x][GEO_TILESIZE - 1]) > DIFF)
				south = false;
			if (Math.abs(tile[0][x] - other.tile[GEO_TILESIZE - 1][x]) > DIFF)
				west = false;
			if (Math.abs(other.tile[0][x] - tile[GEO_TILESIZE - 1][x]) > DIFF)
				east = false;
		}

		if (north)
			validTiles[NORTH].addElement(other);
		if (east)
			validTiles[EAST].addElement(other);
		if (south)
			validTiles[SOUTH].addElement(other);
		if (west)
			validTiles[WEST].addElement(other);

		return north | south | east | west;
	}

	/**
	 * given a vector of tiles set up our own compatibility lists
	 */
	public void findAllCompatibleTiles(Vector tiles) {
		Enumeration e = tiles.elements();

		while (e.hasMoreElements()) 
			checkCompatibility((GeoTile)e.nextElement());
	}

	/**
	 * produce debugging output for this tile
	 */
	public void asciiDump() {
		for (int i = 0; i < GEO_TILESIZE; i++) {
			System.out.print("[");
			for (int j = 0; j < GEO_TILESIZE; j++) {
				System.out.print(" " + tile[i][j]);
			}
			System.out.println(" ]");
		}

		if (hasCity)
			System.out.print("City at: ");
		else
			System.out.print("City potentially at: ");

		System.out.println("(" + cityX + ", " + cityY + ")");

		for (int dir = 0; dir < 4; dir++) {
			System.out.println("Connected to " + validTiles[dir].size() +
								" in d=" + dir);
		}
	}
}
