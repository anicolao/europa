package games.Battle.client.loader;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.lang.reflect.*;
import java.net.*;
import java.util.*;
import javax.swing.*;

/**
 * An interface to identify classes that can be interrupted.
 **/
interface Haltable {

    /**
     * Indicate that the current in-progress action should be halted.
     **/
    public void halt();
}

/**
 * The main loader class.
 **/
public class EuropaLoader implements Runnable, Haltable {

    private static final int ITIME = 500;

    /**
     * The base path where the files are stored on the default server.
     **/
    private static final String DEFAULT_SERVER_BASE_PATH = 
                        "http://europa.mochasoft.ca/client/";

    /**
     * The name of the loader properties file.
     **/
    private static final String LOADER_PROPERTIES_FILENAME = 
                                                       "EuropaLoader.properties";

    /**
     * The command-line arguments.
     **/
    private String[] _args;

    /**
     * The loader properties file contents.
     **/
    private static Properties _loaderProperties;

    /**
     * A flag to indicate that the progress windows have been closed and 
     * the game should just be launched.
     **/
    private boolean _interrupted;

    /**
     * Create the instance.
     *
     * @param args The command-line arguments.
     **/
    private EuropaLoader( String[] args ) {
        _args = args;
    }

    /**
     * Command line entry point.
     *
     * @param args Command-line arguments.
     **/
    public static void main( String[] args ) {
	try {
	    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	}
	catch( Exception ie ) {
	    System.out.println("failed to set look of application"); 
	}

        new EuropaLoader( args ).run();
    }

    /* Implementation of Runnable.run */
    public void run() {

        // Ignore exceptions thrown by the loader updating the files
        try {
            loadProperties();

            if( updateCheckRequired() ) {
                loadUpdatedFiles();
            }
	    saveProperties();
        } catch( Throwable e ) {
	    System.err.println("Exception: " + e);
	    e.printStackTrace();

        }

        try {
            launchGame( _args );
        } catch( Throwable e ) {
	    System.out.println("Exception: " + e);
	    e.printStackTrace();
            JOptionPane.showMessageDialog( null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE );
            System.exit( 0 );
        }
    }

    /**
     * Load the properties files.
     **/
    private void loadProperties() {
        _loaderProperties = loadProperties( LOADER_PROPERTIES_FILENAME );
    }

    /**
     * Save the properties.
     **/
    private void saveProperties() {
        saveProperties( _loaderProperties, LOADER_PROPERTIES_FILENAME, "Loader Properties File" );
    }

    static String installedDirectory;
    static public String getInstalledDirectory() {
	String ret = installedDirectory;
	if (_loaderProperties != null) 
	    ret = _loaderProperties.getProperty( "loader.installdir", installedDirectory );
	return ret;
    }

    static public File getInstalledDirectoryFile() {
	return new File(getInstalledDirectory());
    }

    public boolean isInstalling() {
	return installedDirectory != null;
    }

    static public String fixBackslashes(String input) {
	StringBuffer ret = new StringBuffer();
	int len = input.length();
	for (int i = 0; i < len; ++i) {
	    if (input.charAt(i) == '\\') {
		ret.append(input.charAt(i));
		ret.append(input.charAt(i));
	    }
	    else
		ret.append(input.charAt(i));
	}
	return ret.toString();
    }

    /**
     * Load a properties file from the install directory.
     *
     * @param name The name of the properties file to load.
     *
     * @return The properties contents.
     **/
    public static Properties loadProperties( String name ) {
        Properties properties = new Properties();

	InputStream fis = System.class.getResourceAsStream("/" + name);
        if( fis != null ) {
            try {

                properties.load( fis );
            } catch( IOException e ) {
                properties = new Properties();
            }
        }
	else {
	    /*
            JOptionPane opts = new JOptionPane("Choose a directory to install to, or click cancel to end without installing Europa", JOptionPane.OK_CANCEL_OPTION);
	    opts.setWantsInput(true);
	    JDialog d = opts.createDialog(null, "Install Europa");
	    d.show();
	    */

	    String base = System.getProperty("user.home");
	    base = base + System.getProperty("file.separator") + "Europa";
	    while (true) {
		JFileChooser fd = new JFileChooser(base);
		fd.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fd.setMultiSelectionEnabled(false);
		int ret = fd.showDialog(null, "Install Europa in this Folder");
		if (ret != fd.APPROVE_OPTION) {
		    JOptionPane.showMessageDialog( null, "Installation cancelled.", "Error", JOptionPane.ERROR_MESSAGE );
		    System.exit(1);
		}
		if (fd.getSelectedFile().exists()) {
		    JOptionPane.showMessageDialog( null, "Please choose a directory name that does not conflict with an existing file.", "Can't install in "+fd.getSelectedFile().toString(), JOptionPane.ERROR_MESSAGE );
		    continue;
		}
		installedDirectory = fd.getSelectedFile().toString();
		break;
	    }
	    System.err.println("Setting install dir to: " + installedDirectory);
	    getInstalledDirectoryFile().mkdirs();

	    System.err.println("Writing PlayEuropa script");
	    String script = installedDirectory + 
	                    System.getProperty("file.separator") + 
			    "PlayEuropa";
	    String cp = installedDirectory + 
		        System.getProperty("path.separator") + 
		        installedDirectory + 
		        System.getProperty("file.separator") +
		        "EuropaLoader.jar";
	    try {
		File sf = new File(script);
		PrintStream ps = new PrintStream(new FileOutputStream(sf));
		ps.println("#!/bin/sh");
		ps.println(System.getProperty("java.home") +
			       System.getProperty("file.separator") + "bin" +
			       System.getProperty("file.separator") + "java " +
			       "-cp " + cp +
			       " games.Battle.client.loader.EuropaLoader $*");
		ps.close();
	    }
	    catch (IOException e) {
		System.out.println("exception : " + e);
		JOptionPane.showMessageDialog( null, "File read/write error. Perhaps your disk is full?\nClear some space and try again.", "Error", JOptionPane.ERROR_MESSAGE );
		System.exit(1);
		
	    }
	    try {
		Runtime.getRuntime().exec("chmod +x " + script);
	    }
	    catch (Exception err) {}
	}

        return properties;
    }
    /**
     * Save the given properties to the given file in the installation directory.
     *
     * @param properties The properties to save.
     * @param name The name of the file to save them to.
     * @param title The title to associate with the properties file.
     **/
    public static void saveProperties( Properties properties, String name, String title ) {
        File propertiesFile = new File( getInstalledDirectory(), name );

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            properties.store( bos, title );
            bos.flush();
            byte[] data = bos.toByteArray();
            bos.close();

            FileOutputStream fos = new FileOutputStream( propertiesFile );
            fos.write( data );
            fos.flush();
            fos.close();
        } catch( IOException e ) {
        }
    }





    /**
     * Launch the game.
     *
     * @param args Command-line arguments.
     *
     * @throws Exception Thrown if there is a problem launching the class.
     **/
    private void launchGame( String[] args ) throws Exception {
        launchMain( _loaderProperties.getProperty( "main.jar" ), _loaderProperties.getProperty( "main.class" ), args );
    }

    /**
     * Launch the application in the given class and jar file.
     *
     * @param jarFile The name of the jar file.
     * @param className The name of the class in the jar file.
     * @param args The arguments to pass.
     *
     * @throws Exception Thrown if there is a problem launching the class.
     **/
    private void launchMain( String jarFile, String className, String[] args ) throws Exception {
        try {
            File           installDir = getInstalledDirectoryFile();
            //JarClassLoader classloader = new JarClassLoader( new File( installDir, jarFile ) );
	    StringTokenizer tokenizer = new StringTokenizer( getRequiredFiles(), " \n\r" );

	    while( tokenizer.hasMoreElements() ) {
		String filename = tokenizer.nextToken();
		File   file = new File( installDir, filename );
		try {
		    urls.addElement(file.toURL());
		}
		catch (MalformedURLException mue) {
		    System.err.println("not reached: malformed filename");
		}

	    }
	    URL[] urla = new URL[urls.size()];
	    urls.toArray(urla);

	    URLClassLoader classloader = new java.net.URLClassLoader(urla);
            Class          h = classloader.loadClass( className );
            Class[]        launchArgs = new Class[1];

            launchArgs[0] = args.getClass();

            Method         mainm = h.getDeclaredMethod( "main", launchArgs );
            Object[]       pargs = new Object[1];

            pargs[0] = args;
            mainm.invoke( h, pargs );
        } catch( Throwable e ) {
            throw new Exception( "Can't launch class: " + e );
        }
    }

    /**
     * Determine the list of required files.
     *
     * @return The list of required files.
     **/
    private String getRequiredFiles() {
        String ret = _loaderProperties.getProperty( "required.files" );
	if (isInstalling()) {
	    ret = ret + "EuropaLoader.jar\n";
	}
	return ret;
    }

    Vector urls = new Vector(5);

    /**
     * Check if an update is required.  An update check is required if:
     * <p>
     * <ul>
     * <li>the loader properties file is missing,
     * <li>the time between updates is too long, or
     * <li>one or more required game files are missing.
     * </ul>
     *
     * @return true if an update check is required.
     **/
    private boolean updateCheckRequired() {

        // Check for the existence of the loader properties file
        if( _loaderProperties.size() == 0 ) {
            return true;
        }

        // Check for missing files
        StringTokenizer tokenizer = new StringTokenizer( getRequiredFiles(), " \n\r" );
        File            installDir = getInstalledDirectoryFile();

        while( tokenizer.hasMoreElements() ) {
            String filename = tokenizer.nextToken();
            File   file = new File( installDir, filename );

            if( !file.exists() ) {
                return true;
            }
        }

	return loaderPropertiesChanged();
    }

    boolean gaveUp = true;
    boolean propertiesChanged = false;
    /**
     * Determine if the loader properties file has changed.
     *
     * @return true if the loader properties file has changed.  The loader 
     * properties are also refreshed if they have changed.
     **/
    private boolean loaderPropertiesChanged() {
	Runnable checkProps = new Runnable() {
	    public void run() {
		try {
		    URL           url = new URL( DEFAULT_SERVER_BASE_PATH + LOADER_PROPERTIES_FILENAME );
		    URLConnection connection = url.openConnection();

		    long lastMod = connection.getLastModified();
		    if( lastMod != getPropertiesTime() ) {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			getFileFromServer( connection, bos, null );

			String idir = getInstalledDirectory();
			_loaderProperties = new Properties();
			_loaderProperties.load( new ByteArrayInputStream( bos.toByteArray() ) );
			_loaderProperties.setProperty( "loader.time", "" + lastMod );
			_loaderProperties.setProperty( "loader.waittime", "" + ITIME );
			_loaderProperties.setProperty( "loader.installdir", idir );
			bos.close();

			propertiesChanged = true;
		    }
		} catch( IOException e ) {
		    e.printStackTrace( System.err );
		}
		finally {
		    gaveUp = false;
		}
	    }
	};
	Thread propertyCheck = new Thread(checkProps);
	propertyCheck.start();
	try {
	    propertyCheck.join(getWaitTime());
	}
	catch (InterruptedException e) {
	}
	if (gaveUp) {
	    System.out.println("Gave up waiting for network; starting game.");
	    long wait = getWaitTime() * 2;
	    if (wait > 10000) wait = 10000;
	    _loaderProperties.setProperty( "loader.waittime", "" + wait );
	}

        return propertiesChanged;
    }

    /**
     * Center the given window.
     *
     * @param window The window to center on the screen.
     **/
    private void centerWindow( JFrame window ) {
        Dimension windowPreferredSize = window.getPreferredSize();
        Dimension windowSize = window.getSize();

        if( windowSize.width < windowPreferredSize.width ) {
            windowSize.width = windowPreferredSize.width;
        }
        if( windowSize.height < windowPreferredSize.height ) {
            windowSize.height = windowPreferredSize.height;
        }

        window.setSize( windowSize );
        Dimension screenSize = window.getToolkit().getScreenSize();

        window.setLocation( (screenSize.width - windowSize.width) / 2, (screenSize.height - windowSize.height) / 2 );
    }

    public long getWaitTime() {
	String time = _loaderProperties.getProperty( "loader.waittime", "0" );
	try {
	    Long ti = Long.valueOf(time);
	    return ti.longValue();
	}
	catch (NumberFormatException nfe) {}
	return ITIME*10;
    }
    public long getPropertiesTime() {
	String time = _loaderProperties.getProperty( "loader.time", "0" );
	try {
	    Long ti = Long.valueOf(time);
	    return ti.longValue();
	}
	catch (NumberFormatException nfe) {}
	return 0;
    }
    /**
     * Check for game updates.
     *
     * @return true if any of the files were updated.
     *
     * @throws IOException Thrown if there are problems retrieving the update.
     **/
    private boolean loadUpdatedFiles() throws IOException {
        try {
            File      parentDir = getInstalledDirectoryFile();
            Vector    toUpdate = new Vector();
            Vector    connections = new Vector();
            Container contentPane;

            JFrame frame = new JFrame( _loaderProperties.getProperty( "loader.dialog.title", "Europa" ) );
            frame.addWindowListener( new HaltActionOnClose( this ) );

            JLabel checkingLabel = new JLabel( "Checking: EuropaLoader.properties" );
            contentPane = frame.getContentPane();
            contentPane.add( new JLabel( "Checking for updates..." ), BorderLayout.NORTH );
            contentPane.add( checkingLabel, BorderLayout.CENTER );
            frame.pack();
            centerWindow( frame );
            frame.show();

            loaderPropertiesChanged();

            String server = _loaderProperties.getProperty( "default.server", DEFAULT_SERVER_BASE_PATH );

            // For each file in the list, check if it requires updating
            int             updateSize = 0;
            StringTokenizer tokenizer = new StringTokenizer( getRequiredFiles(), " \n\r" );
            while( tokenizer.hasMoreElements() ) {
                String filename = tokenizer.nextToken();
                checkingLabel.setText( "Checking: " + filename );
                centerWindow( frame );

                URL           url = new URL( server + filename );
                URLConnection connection = url.openConnection();
                File          localFile = new File( parentDir, filename );

                if( !localFile.exists() || localFile.lastModified() < connection.getLastModified() ) {
                    toUpdate.addElement( filename );
                    connections.addElement( connection );
                    updateSize += connection.getContentLength();
                }
                if( _interrupted ) {
                    return false;
                }
            }
            frame.hide();

            // Now download each of the files
            int toUpdateCount = toUpdate.size();
            if( toUpdateCount > 0 ) {
                JFrame           progressFrame = new JFrame( "Downloading..." );
                DownloadProgress dp = new DownloadProgress( updateSize );
                JLabel           currentFile = new JLabel( "Retrieving: " );

                progressFrame.addWindowListener( new HaltActionOnClose( this ) );
                contentPane = progressFrame.getContentPane();
                contentPane.add( currentFile, BorderLayout.NORTH );
                contentPane.add( dp, BorderLayout.CENTER );
                progressFrame.pack();
                centerWindow( progressFrame );
                progressFrame.show();

                // Grab all of the files now
                for( int i = 0; !_interrupted && i < toUpdateCount; ++i ) {
                    String filename = (String) toUpdate.elementAt( i );
                    currentFile.setText( "Retrieving: " + filename );
                    centerWindow( progressFrame );

                    URLConnection    connection = (URLConnection) connections.elementAt( i );
                    File             targetFile = new File( parentDir, filename + ".tmp" );
                    FileOutputStream fos = new FileOutputStream( targetFile );

                    getFileFromServer( connection, fos, dp );
                    fos.close();
                    targetFile.deleteOnExit();
                }

                // Rename them
                currentFile.setText( "Installing updates..." );
                centerWindow( progressFrame );
                for( int i = 0; i < toUpdateCount; ++i ) {
                    String filename = (String) toUpdate.elementAt( i );
                    File   srcFile = new File( parentDir, filename + ".tmp" );
                    File   destFile = new File( parentDir, filename );

                    if( _interrupted ) {
                        srcFile.delete();
                    } else {
                        if( destFile.exists() ) {
                            destFile.delete();
                        }

                        srcFile.renameTo( destFile );
                    }
                }

                return !_interrupted;
            }
        } catch( IOException e ) {
            e.printStackTrace( System.err );
            throw new IOException( "Unable to load update from server (" + e.getMessage() + ")" );
        }

        return false;
    }

    /**
     * Get a file from the server.
     *
     * @param connection The connection to the data from.
     * @param outputStream Where to write the file to.
     * @param dp Download progress status.  May be null.
     *
     * @throws IOException Thrown if there are problems downloading the file from the server.
     **/
    private void getFileFromServer( URLConnection connection, OutputStream outputStream, DownloadProgress dp ) throws IOException {
        byte[]        buffer = new byte[8 * 1024];
        int           size = connection.getContentLength();
        InputStream   is = connection.getInputStream();

        int totalReadCount = 0;
        int readCount;
        while( totalReadCount < size && (readCount = is.read( buffer )) > -1 ) {
            if( totalReadCount + readCount > size ) {
                readCount = size - totalReadCount;
            }

            outputStream.write( buffer, 0, readCount );
            totalReadCount += readCount;

            if( dp != null ) {
                dp.addBytes( readCount );
            }
        }

        outputStream.flush();
    }

    /* Implementation of Haltable.halt */
    public void halt() {
        _interrupted = true;
    }

    /**
     * Listen for the window to close and then halt the given object.
     **/
    private static class HaltActionOnClose extends WindowAdapter {

        /**
         * The action to halt.
         **/
        private Haltable _haltable;

        /**
         * Create the instance.
         *
         * @param haltable The action that should be halted.
         **/
        HaltActionOnClose( Haltable haltable ) {
            _haltable = haltable;
        }

        /* Override of WindowListenerAdapter.windowClosing */
        public void windowClosing( WindowEvent event ) {
            _haltable.halt();
        }
    }
}
