package games.Battle.client.loader;

import java.awt.*;
import java.util.*;
import javax.swing.*;

class PercentBar extends JPanel {
    double percent;

    public PercentBar(double percent) {
	this.percent = percent;
    }

    public void setPercent(double percent) {
	this.percent = percent;
	repaint();
    }

    public void update(Graphics g) {
	paint(g);
    }

    public void paint(Graphics g) {
	Rectangle bounds = getBounds();
	g.setColor(Color.black);
	g.fillRect( bounds.x, bounds.y, bounds.width, bounds.height );
	g.setColor(Color.green);
	g.fillRect( bounds.x, bounds.y, (int)(percent*bounds.width), bounds.height );
    }

    public Dimension getMinimumSize() {
	return getPreferredSize();
    }

    public Dimension getPreferredSize() {
	return new Dimension(200, 4);
    }
}

public class DownloadProgress extends JPanel {
    int totalBytes;
    int bytesSoFar;
    JLabel elapsed;
    JLabel guessed;
    Date start;
    double lastGuessedPercent = -10;
    int lastGuessed = 0;

    PercentBar pb;
    Thread recalcThread;
    public DownloadProgress(int tb) {
	this.totalBytes = tb;
	this.bytesSoFar = 0;
	pb = new PercentBar(0);
	setLayout(new BorderLayout());
	add(pb, "North");
	JPanel textPanel = new JPanel();
	textPanel.add(new JLabel("Time: "));
	elapsed = new JLabel("0:00");
	textPanel.add(elapsed);
	textPanel.add(new JLabel("/"));
	guessed = new JLabel("?:??");
	textPanel.add(guessed);
	start = new Date();
	add(textPanel, "Center");
	Runnable recalc = new Runnable() {
	    public void run() {
		while (bytesSoFar < totalBytes) {
		    try {
			Thread.sleep(1000);
			recalculate();
		    }
		    catch (InterruptedException e) {
			return;
		    }
		}
		recalculate();
		Component c = getParent();
		while (!(c instanceof Frame)) 
		    c = c.getParent();
		Frame p = (Frame)c;
		p.hide();
		p.dispose();
	    }
	};
	recalcThread = new Thread(recalc);
	recalcThread.start();
    }

    public void setBytesSoFar(int bytesSoFar) {
	this.bytesSoFar = bytesSoFar;
	recalculate(); 
    }

    public void addBytes( int count ) {
        setBytesSoFar( bytesSoFar + count );
    }

    public void recalculate() {
	double percent = bytesSoFar / (double)totalBytes;
	pb.setPercent(percent);
	Date now = new Date();
	long tsec = (now.getTime() - start.getTime())/1000;
	long minutes = tsec / 60;
	long seconds = tsec % 60;
	if (seconds > 9)
	    elapsed.setText("" + minutes + ":" + seconds);
	else
	    elapsed.setText("" + minutes + ":0" + seconds);
	int totalTime;
	if (percent > 0.01) 
	    totalTime = (int)(tsec / percent);
	else
	    totalTime = 0;
	if (percent - lastGuessedPercent > 0.1 || totalTime > lastGuessed) {
	    lastGuessed = totalTime;
	    lastGuessedPercent = percent;
	    int m = totalTime / 60;
	    int s = totalTime % 60;
	    if (s > 9)
		guessed.setText("" + m + ":" + s);
	    else
		guessed.setText("" + m + ":0" + s);
	}
	repaint();
    }
}
