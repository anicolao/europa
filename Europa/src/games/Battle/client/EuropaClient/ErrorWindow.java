/*
 * @(#)ErrorWindow.java
 */

package games.Battle.client.EuropaClient;

import java.awt.*;

/**
 * Implements a small modal dialog box which displays a single
 * line of text along with an "ok" box. When executed, the
 * calling instance waits, and the dialog 
 * does not return control until the "ok" button is pressed.
 *
 * @author Alex Nicolaou
 * @author Jay Steele
 */
public class ErrorWindow extends Frame {

	/**
	 * The main client applet.
	 */
	EuropaClient client;

	/**
	 * The ok button.
	 */
	Button ok;

	/**
	 * The string inside the okay button.
	 */
	String OK = "    Ok    ";

	/**
	 * Constructs a Error window as a child of the given client
	 * and displaying the given message.
	 * @param client the client which is waiting for the dialog
	 * @param msg the error message to display
	 */
	public ErrorWindow(EuropaClient client, String msg) {
		//super(null, "Error", true);
		super("Error");
		this.client = client;

		Panel message = new Panel();
		message.setLayout(new FlowLayout(FlowLayout.CENTER, 50, 20));
		message.add(new Label(msg));
		add("North", message);

		Panel buttons = new Panel();
		buttons.setLayout(new FlowLayout(FlowLayout.CENTER, 50, 20));
		buttons.add(ok = new Button(OK));
		add("South", buttons);

		pack();

		ok.requestFocus();
	}

	/**
	 * Handle the "ok" button.
	 */
    public boolean handleEvent(Event e) {
        if (e.id == Event.WINDOW_DESTROY || e.arg == OK) {
			client.dialogDone();
			dispose();
        }

		return super.handleEvent(e);
    }

	/**
	 * Overload the layout() method to additionally call pack().
	 */
	public synchronized void layout() {
		// pack();
		super.layout();
	}
}
