/*
 * @(#)EuropaQueueComponent.java
 */

package games.Battle.client.EuropaClient;

import java.awt.*;

import games.Battle.shared.sys.Symbols;
import games.Battle.shared.comm.*;
import games.Battle.client.ClientApplet.ClientImages;

/**
 * The EuropaQueueComponent is a graphical representation of a 
 * QueueComponent, giving visual feedback to players on who
 * is currently waiting for or currently playing a game on 
 * the server.
 *
 * This implementation displays an image of the moon in the center
 * of the component with some number of "satellites" surrounding
 * the moon. When a player joins a game, his or her name
 * is added to a satellite with an appropriate player color.
 * 
 * The player ranking is also displayed below the name.
 *
 * @author Alex Nicoloau
 * @author Jay Steele
 */
public class EuropaQueueComponent extends QueueComponent 
{
	/**
	 * The media tracker used to wait for the Europa moon image
	 */
	MediaTracker tracker = null;

	/**
	 * The Europa moon image.
	 */
	Image europa = null;

	/**
	 * The maxium number of players in thie queue.
	 */
	int maxPlayers;

	/**
	 * An array representing the positions of all the satellites.
	 */
	Point[] satPos;

	/**
	 * An array of string for the names of player attached to satellites.
	 */
	String[] satTitle;

	/**
	 * An array of string for the ratings of player attached to satellites.
	 */
	String[] satRating;

	/**
	 * Create a EuropaQueueComponent with the given GameInfo and 
	 * EuropaClient. Initializes the locations of the satellites
	 * in the square and initiazes all of the display strings to
	 * nothing.
	 * @param inf the game info
	 * @param client the "owner" client applet
	 */
	public EuropaQueueComponent(GameInfo inf, EuropaClient client) {
		super(inf, client);

		maxPlayers = game.numberNeeded();

		satPos = new Point[maxPlayers];
		satTitle = new String[maxPlayers];
		satRating = new String[maxPlayers];

		tracker = new MediaTracker(this);
		europa = ClientImages.getImage(client, "europa.gif");
		setBackground(Color.lightGray);
		tracker.addImage(europa, 0);

		// wait for the image to load
		try {
			tracker.waitForID(0);
		} 
		catch (InterruptedException e) { 
		}

		initSatellites();

		initFromGame();

		setBackground(Color.black);
	}

	/**
	 * resets all the strings that are displayed in this component 
	 */
	void initFromGame() {
		maxPlayers = game.numberNeeded();
		
		for (int i=0; i<maxPlayers; i++) {
			satTitle[i] = new String("");
			satRating[i] = new String("");
		}

		for (int i = 0; i < game.numberJoined(); i++) {
			satTitle[i] = game.players[i].getHandle();
			satRating[i] ="["+game.players[i].getRating().toString() + "]";
		}
	}

	/**
	 * make the component up to date before repainting with superclass method
	 */
	public void resetGameInfo(GameInfo g) {
		game = g;
		initFromGame();
		super.resetGameInfo(g);
	}

	/**
	 * Initialize all of the satellite positions in the panel. They
	 * are arranged in a small semicircle to the left of the planet
	 * to leave room for the name text.
	 */
	void initSatellites() {
		int xradius = (int)((double)(getBounds().width)/2 - 10.0);
		int yradius = (int)((double)(getBounds().height)/2 - 10.0);
		int cx = (int)((double)getBounds().width / 2.0);
		int cy = (int)((double)getBounds().height / 2.0);
		double range = (Math.PI/1.5);
		double thetaseg = range / (double)(maxPlayers-1);
		for (int i=0; i<maxPlayers; i++) {
			double theta = -((double)i * thetaseg + (Math.PI/2)) 
				- ((Math.PI-range)/2);
			int x = (int)(Math.cos(theta) * xradius) + cx;
			int y = (int)(Math.sin(theta) * yradius) + cy;
			satPos[i] = new Point(x, y);
		}
	}

	static private final Color playerColor[] = {
		new Color(200, 0, 0),
		new Color(0, 200, 0),
		new Color(200, 200, 0),
		new Color(0, 200, 200),
		new Color(200, 0, 200)
	};

	boolean hilit = false;

	/**
	 * Return true if the clients currently executing game is
	 * this component's game id.
	 */
	boolean inThisGame() {
		return (game.getId() == client.getGameId());
	}

	/**
	 * Hilite this component (when the mouse moves inside) with
	 * a "Quit" or "Join" box.
	 * @param g the graphics context to draw on
	 */
	void drawHilite(Graphics g) {
		Rectangle size = getBounds();
		g.drawRect(2, 2, size.width - 5, size.height - 5);
		if (inThisGame() && !game.isRunning())
			g.drawString("Quit", size.width-40, size.height-15);
		else if (game.isInGame(client.getPlayerId()) && game.isRunning())
			return;
		else if (game.isRunning())
			g.drawString("Watch", size.width-50, size.height-15);
		else
			g.drawString("Join", size.width-40, size.height-15);
	}


	static Font helvetica = new Font("Helvetica", Font.PLAIN, 12);
	/**
	 * Paints the panel. Any satellites with no label are painted
	 * gray, all others are painted their appropriate player color.
	 * @param g the graphics context to draw on
	 */
	public void paint(Graphics g) {
		initSatellites();

		g.setFont(helvetica);

		g.setColor(Color.black);
		Rectangle size = getBounds();
		//g.fillRect(0, 0, size.width, size.height);

		int w = (int)((double)(getBounds().width - europa.getWidth(this)) / 2.0);
		int h = (int)((double)(getBounds().height - europa.getHeight(this)) / 2.0);
		g.drawImage(europa, w, h, this);

		for (int i=0; i<maxPlayers; i++) {
			if (satTitle[i].equals("")) {
				g.setColor(Color.lightGray);
			} else {
				g.setColor(playerColor[i]);
				g.drawString(satTitle[i], satPos[i].x+5, satPos[i].y);
				g.drawString(satRating[i], satPos[i].x+5, satPos[i].y + 13);
			}
			g.fillOval(satPos[i].x-2, satPos[i].y-2, 4, 4);
		}

		if (hilit) {
			if (inThisGame() && !game.isRunning())
				g.setColor(Color.red);
			else if (game.isInGame(client.getPlayerId()) && game.isRunning())
				g.setColor(Color.gray);
			else
				g.setColor(Color.green);
			drawHilite(g);
		}
	}

	/**
	 * Hilite this component with the appropriate color and
	 * draw.
	 */
	public void hilite() {
		hilit = true;
		Graphics g = getGraphics();
		g.setFont(helvetica);
		if (inThisGame())
			g.setColor(Color.red);
		else
			g.setColor(Color.green);
		drawHilite(g);
	}

	/**
	 * Erase any previous hilites in this component by drawing
	 * in black.
	 */
	public void normal() {
		hilit = false;
		Graphics g = getGraphics();
		g.setFont(helvetica);
		g.setColor(Color.black);
		drawHilite(g);
	}

	public Dimension minimumSize() {
		return new Dimension(100, 60);
	}

	public Dimension preferredSize() {
		return new Dimension(140, 100);
	}

}
