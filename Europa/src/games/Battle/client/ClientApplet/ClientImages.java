/*
 * @(#)ClientImages.java
 */

package games.Battle.client.ClientApplet;

import java.applet.*;
import java.awt.*;
import java.awt.image.*;
import java.net.*;
import java.util.Random;

import games.image.*;
import games.Battle.shared.sys.*;

/**
 * ClientImages is responsible for loading and building all of the
 * client images for the terrain, troops, water, etc. The class
 * retains a collection of public static images, which are
 * initialized with the call loadAll(applet). 
 *
 * ClientImages loads a minimal set of images from the client URL
 * and builds all of the other images from this original set with
 * image processing routines.
 *
 * @author Alex Nicolaou
 * @author Jay Steele
 */

public class ClientImages {

	/**
	 * The applet representing the source of the URL.
	 */
	public static Applet applet = null;

	/**
	 * The directory containing the images.
	 */
	private static String imageDir = "images";

	/**
	 * The media tracker used to wait for all the images.
	 */
	private static MediaTracker tracker1 = null;

	/**
	 * The URL extracted from the applet.
	 */
	private static URL docBase = null;

	/**
	 * The mediatracker image id.
	 */
	static int id = 0;

	/**
	 * The background earth or floor image
	 */
	public static Image earth = null;

	/**
	 * The 4 horizontal hilite images.
	 */
	public static Image hilite_h[] = new Image[4];

	/**
	 * The 4 vertical hilite images.
	 */
	public static Image hilite_v[] = new Image[4];

	/**
	 * The 4 horizontal shadow images.
	 */
	public static Image shadow_h[] = new Image[4];

	/**
	 * The 4 vertical shadow images.
	 */
	public static Image shadow_v[] = new Image[4];

	/**
	 * The 3 smoke images.
	 */
	public static Image smoke[] = new Image[3];

	/**
	 * The 3 fire images.
	 */
	public static Image fire[] = new Image[3];

	/**
	 * The 4 rotated single water puddle images.
	 */
	public static Image water_single[] = new Image[4];

	/**
	 * The 4 rotated straight shaped water images.
	 */
	public static Image water_straight[] = new Image[2];

	/**
	 * The 4 rotated elbow shaped water images.
	 */
	public static Image water_elbow[] = new Image[4];

	/**
	 * The 4 rotated T shaped water images.
	 */
	public static Image water_t[] = new Image[4];

	/**
	 * The 4 rotated end water images.
	 */
	public static Image water_end[] = new Image[4];

	/**
	 * The single cross shaped water image.
	 */
	public static Image water_cross = null;

	/**
	 * The city image.
	 */
	public static Image city = null;

	/**
	 * A paratrooper image.
	 */
	public static Image paratrooper = null;

	/**
	 * The four rotated gun images.
	 */
	public static Image gun[] = new Image[4];

	/**
	 * The four rotated smudge images.
	 */
	public static Image smudge[] = new Image[4];


	/**
	 * A sanity check to absolutely ensure all of the images
	 * have been loaded properly before the client is allowed
	 * to execute. The method basically just polls the width and
	 * height of the image forever until a valid result is returned.
	 * @param im the image to poll
	 */
	static void pollImage(Image im) {
		MediaTracker t = new MediaTracker(applet);
		t.addImage(im, 0);
		try {
		    t.waitForAll();
		}
		catch(Exception e) {
		}
		while (im.getWidth(applet) == -1) {
			try {
				Thread.sleep(100);
			}
			catch (Exception e) {}
		}
		while (im.getHeight(applet) == -1) {
			try {
				Thread.sleep(100);
			}
			catch (Exception e) {}
		}
	}

	static Component component;
	static private Image getImage(String name) {
	    return getImage(component, name);
	}
	/**
	 * Retrieves an image. This method look after ensuring the 
	 * image is loaded from the correct URL, and the pathname
	 * of the image.
	 * @param name the name of the file to get
	 */
	public static Image getImage(Component c, String name) {
		Image im = null;
		try {
		    URL imurl = c.getClass().getResource("/images/" + name);
		    if (imurl != null) {
			Object imobj = imurl.getContent();
			im = c.createImage((ImageProducer)imobj);
		    }
		    else {
			im = ((Applet)c).getImage(docBase, "images" + "/" + name);
		    }
		    tracker1.addImage(im, 0);
		    c.prepareImage(im, c);
		    pollImage(im);
		}
		catch (java.io.IOException e) {
		    System.out.println("Exception caught loading " + name);
		}
		return im;
	}

	/**
	 * Load the background "earth" image. This image is 128x128 and
	 * placed in a 4x4 grid on the background of the client applet.
	 */
	private static void loadEarthImages() {
		earth = getImage("earth.gif");
	}

	/**
	 * Load the hilite and shadow images.
	 */
	private static void loadHiliteAndShadowImages() {
		hilite_h[0] = getImage("hilite.gif");
		shadow_h[0] = getImage("shadow.gif");
	}

	/**
	 * Load the smoke images.
	 */
	private static void loadSmokeImages() {
		for (int i=0; i<3; i++) {
			smoke[i] = getImage("smoke"+i+".gif");
		}
	}

	/**
	 * Load the fire images.
	 */
	private static void loadFireImages() {
		for (int i=0; i<3; i++) {
			fire[i] = getImage("fire"+i+".gif");
		}
	}

	/**
	 * Load the water images.
	 */
	private static void loadWaterImages() {
		water_cross 		= getImage("water_cross.gif");
		water_single[0] 	= getImage("water_single.gif");
		water_elbow[0] 		= getImage("water_elbow.gif");
		water_t[0] 			= getImage("water_t.gif");
		water_end[0] 		= getImage("water_end.gif");
		water_straight[0] 	= getImage("water_straight.gif");
	}

	/**
	 * Load the city images.
	 */
	public static void loadCityImages() {
		city = getImage("city.gif");
	}

	/**
	 * Load the paratrooper and gun images.
	 */
	public static void loadParatrooperAndGunImages() {
		paratrooper = getImage("para.gif");
		gun[0] = getImage("gun.gif");
	}

	/**
	 * Load the terrain smudge image.
	 */
	public static void loadSmudgeImages() {
		smudge[0] = getImage("smudge.gif");
	}

	private static Image old_createRotatedImage(Image i) {
		Image ret = applet.createImage( new FilteredImageSource(
											i.getSource(),
											new RotateImageFilter(1) ));

		pollImage(ret);

		return ret;
	}

	/*
	01234
	56789

	49 01 50
	38 23 61
	27 45 27
	16 67 38
	05 89 94

	0 = 4
	1 = 9
	2 = 3;
	3 = 8

	*/

	
	static int[] rot90(int pix[], int h, int w) {
		int sparepix;
		int newpix[] = new int[w*h];

		for (int x = 0; x < w; x++) {
			for (int y = 0; y < h; y++) {
				//newpix[x+y*w] = pix[(h - 1 - y) + (x*h)];
				newpix[x+y*w] = pix[((w-1-x)*h) + y];
			}
		}

		return newpix;
	}

	private static Image createRotatedImage(Image i) {
		int w = i.getWidth(applet);
		int h = i.getHeight(applet);
		int pix[] = new int[w*h];

		PixelGrabber image = new PixelGrabber(i, 0, 0, w, h, pix, 0, w);
		try {
			image.grabPixels();
		}
		catch(Exception e) {
			System.out.println("error creating images");
		}

		pix = rot90(pix, w, h);
		Image ret = applet.createImage(new MemoryImageSource(h, w, pix, 0, h));

		pollImage(ret);
		return ret;
	}

	/**
	 * From the original horizontal hilite and shadow images, build the 
	 * vertical versions by rotating the original.
	 */
	private static void buildHiliteAndShadowImages() {
		int w = hilite_h[0].getWidth(applet);
		int h = hilite_h[0].getHeight(applet);

		for (int i=1; i<4; i++) {
			hilite_h[i] = applet.createImage(
							new FilteredImageSource(
								hilite_h[0].getSource(),
								new CropImageFilter(0, i, w, h-(i*2)) ));
			pollImage(hilite_h[i]);
		}
		for (int i=1; i<4; i++) {
			shadow_h[i] = applet.createImage(
							new FilteredImageSource(
								shadow_h[0].getSource(),
								new CropImageFilter(0, i, w, h-(i*2)) ));
			pollImage(shadow_h[i]);
		}
		for (int i=0; i<4; i++) {
			hilite_v[i] = createRotatedImage(hilite_h[i]);
			shadow_v[i] = createRotatedImage(shadow_h[i]);
		}
	}

	/**
	 * Build all variations of the water images by rotating the
	 * originals to create the new images.
	 */
	private static void buildWaterImages() {
		for (int i=1; i<4; i++) {
			water_single[i] = createRotatedImage(water_single[i-1]);
			water_elbow[i] = createRotatedImage(water_elbow[i-1]);
			water_t[i] = createRotatedImage(water_t[i-1]);
			water_end[i] = createRotatedImage(water_end[i-1]);
		}

		for (int i=1; i<2; i++) {
			water_straight[i] = createRotatedImage(water_straight[i-1]);
		}
	}

	/**
	 * Build all variations of the gun images by rotating
	 * the original gun image.
	 */
	public static void buildGunImages() {
		for (int i=1; i<4; i++) {
			gun[i] = createRotatedImage(gun[i-1]);
		}
	}

	/**
	 * Build all variations of the terrain smudge images by
	 * rotating the orginal image.
	 */
	public static void buildSmudgeImages() {
		for (int i=1; i<4; i++) {
			smudge[i] = createRotatedImage(smudge[i-1]);
		}
	}

	/**
	 * Build all images. Pass in the applet in order to extract the
	 * originating URL. The method processes images which have already
	 * been loaded in order to create new images.
	 * @param a the applet from which the URL will be extracted
	 */
	public static void buildAll(Applet a) {
		applet = a;
		try {
		    docBase = a.getCodeBase();
		}
		catch(NullPointerException e) {}

		buildHiliteAndShadowImages();
		buildGunImages();
		buildSmudgeImages();
		buildWaterImages();
	}

	/**
	 * Get all of the images required by the client. Pass in the applet
	 * in order to extract the originating URL. The method uses
	 * MediaTracker to ensure the images have completely loaded
	 * before the client application can execute.
	 * @param a the applet from which the URL will be extracted
	 */
	public static void loadAll(Applet a) {
		applet = a;
		component = a;
		try {
		    docBase = a.getCodeBase();
		} catch(NullPointerException e) {}
		tracker1 = new MediaTracker(applet);

		loadEarthImages();
		loadHiliteAndShadowImages();
		loadSmokeImages();
		loadFireImages();
		loadWaterImages();
		loadCityImages();
		loadParatrooperAndGunImages();
		loadSmudgeImages();

		// Wait for all the images to load
		//for (int i=0; i<id; i++) {
		    try {
			tracker1.waitForAll();
			Thread.sleep(100);
		    } catch (InterruptedException e) {
			System.out.println("tracker wait interrupted");
		    }
		//}

		boolean done = false;
		while (!done) {
		    try {
			Thread.sleep(100);
			buildAll(a); 
			done = true;
		    }
		    catch (Exception e) {
			System.out.println("got exception while doing buildall" + e); 
			e.printStackTrace();
		    }
		}
	}
}
