/*
 * @(#)ClientLookTraditional.java
 */

package games.Battle.client.ClientApplet;

import java.awt.Image;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Component;

import games.Battle.shared.sys.Symbols;
import games.Battle.shared.sys.Rules;

/**
 * ClientLookTraditional implements a look mimicing the traditional
 * xbattle look. Hills and valleys are represented by varying contour
 * shades.
 *
 * ClientLookTraditional is the first client we implemented to 
 * begin testing and implenting the game.
 *
 * @author Alex Nicolaou
 * @author Jay Steele
 */
public class ClientLookTraditional extends ClientLook {

	/**
	 * The cached width and height.
	 */
	int width, height;

	/**
	 * Construct a ClientLookTraditional instance with the
	 * given ClientBoardComponent.
	 * @param c the component to draw in
	 */
	public ClientLookTraditional(ClientBoardComponent c) {
		super(c);
	}

	/**
	 * Update terrain does nothing for this look. All drawing
	 * is done in update().
	 */
	public void updateTerrain(ClientBoard b) {
	}

	/**
	 * Update the display for the client board for every turn. For this
	 * look, this method does all the drawing for the client, including
	 * the terrain. It only draws visible squares.
	 */
	public void update(ClientBoard b) {

		// Clear the board black (the invisible color)
		offGC.setColor(Color.black);
		offGC.fillRect(0, 0, Symbols.BOARD_W, Symbols.BOARD_H);

		float w = (float)Symbols.BOARD_W / (float)Rules.cols;
		float h = (float)Symbols.BOARD_H / (float)Rules.rows;
		width = (int)w;
		height = (int)h;

		// Iterate over all the cells and update visible ones
		for (int r=0; r<Rules.rows; r++) {
			for (int c=0; c<Rules.cols; c++) {
				int xorg = (int)(w * (float)c);
				int yorg = (int)(h * (float)r);
				ClientCell cell = (ClientCell)b.getCell(r, c);
				int occ = cell.getOccupancy();
				if (occ != Symbols.INVISIBLE
					&& occ != Symbols.UNMODIFIED)
				{
					paintTerrain(xorg, yorg, cell.getTerrain());
					int city = cell.getCity();
					if (occ != Symbols.UNOCCUPIED) {
						paintTroops(xorg, yorg, occ, cell.getTroops());
						paintPipes(xorg, yorg, cell.getPipeMask());
					}
					if (city > 0) {
						paintCity(xorg, yorg, city);
					}
				}
			}
		}
	}

	static private final float terrainHue = (float)(25.0 / 255.0);
	static private final float terrainSat = (float)(185.0 / 255.0);
	static private final Color[] terrainColor = {
		new Color(0, 0, 170),
		Color.getHSBColor(terrainHue, terrainSat, (float)0.50),
		Color.getHSBColor(terrainHue, terrainSat, (float)0.46),
		Color.getHSBColor(terrainHue, terrainSat, (float)0.42),
		Color.getHSBColor(terrainHue, terrainSat, (float)0.38),
		Color.getHSBColor(terrainHue, terrainSat, (float)0.34),
		Color.getHSBColor(terrainHue, terrainSat, (float)0.30),
		Color.getHSBColor(terrainHue, terrainSat, (float)0.26)
	};

	/**
	 * Paint the terrain of the given cell.
	 * @param xorg the x origin of the cell to draw
	 * @param yorg the y origin of the cell to draw
	 * @param terrain the level of the terrain to draw
	 */
	void paintTerrain(int xorg, int yorg, int terrain) {
		offGC.setColor(terrainColor[terrain]);
		offGC.fillRect(xorg+1, yorg+1, width-1, height-1);
		offGC.setColor(Color.black);
		offGC.drawRect(xorg, yorg, width, height);
	}

	static private final Color playerColor[] = {
		new Color(200, 0, 0),
		new Color(0, 200, 0),
		new Color(200, 200, 0),
		new Color(0, 200, 200),
		new Color(200, 0, 200),
		new Color(0, 0, 200),
	};

	/**
	 * Paint the given number of troops for the player in the supplied
	 * location.
	 * @param xorg the x location to draw the troops
	 * @param yorg the y location to draw the troops
	 * @param player the player occupancy to draw
	 * @param troops the number of troops (a percentage from 0-31) to draw.
	 */
	void paintTroops(int xorg, int yorg, int player, int troops) {
		float percent = (float)(troops+1) / (float)32.0;
		float w = (float)width * percent;
		float h = (float)height * percent;
		offGC.setColor(playerColor[player]);
		float x = (float)(width) / (float)2.0 - w/(float)2.0 + 1;
		float y = (float)(height) / (float)2.0 - h/(float)2.0 + 1;
		offGC.fillRect((int)x+xorg, (int)y+yorg, (int)w, (int)h);
	}

	/**
	 * Draw the graphic for a city on the game board.
	 * @param xorg the x origin of the cell to draw the city in
	 * @param yorg the y origin of the cell to draw the city in
	 * @param city the size of the city
	 */
	void paintCity(int xorg, int yorg, int city) {
		float angle = (float)360 * (float)city/(float)Symbols.MAX_CLIENT_TROOPS;
		offGC.setColor(Color.black);
		offGC.drawArc(xorg+2, yorg+2, width-4, height-4, 0, (int)angle);
		if (city == Symbols.MAX_CLIENT_TROOPS) {
			offGC.drawArc(xorg+3, yorg+3, width-6, height-6, 0, (int)angle);
			offGC.drawArc(xorg+4, yorg+4, width-8, height-8, 0, (int)angle);
		}
	}

	/**
	 * Paint pipes on a particular cell.
	 * @param xorg the x cell origin to paint the pipe
	 * @param yorg the y cell origin to paint the pipe
	 * @param pipes the flags indicating which pipes are on and off
	 * @param player the player (to draw in the correct color)
	 */
	void paintPipes(int xorg, int yorg, int pipes) {
		int wh = (int)((float)width / (float)2.0);
		int hh = (int)((float)height / (float)2.0);
		int wa = (int)((float)width / (float)3.0);
		int ha = (int)((float)height / (float)3.0);
		int wb = (int)((float)width * (float)2.0 / (float)3.0);
		int hb = (int)((float)height * (float)2.0 / (float)3.0);

		if ((pipes & Symbols.PIPE_MASK[Symbols.NORTH]) != 0) {
			offGC.setColor(Color.black);
			offGC.drawLine(xorg+wh-1, yorg+1, xorg+wh-1, yorg+ha);
			offGC.drawLine(xorg+wh+1, yorg+1, xorg+wh+1, yorg+ha);
			offGC.setColor(Color.white);
			offGC.drawLine(xorg+wh, yorg+1, xorg+wh, yorg+ha);
		}

		if ((pipes & Symbols.PIPE_MASK[Symbols.SOUTH]) != 0) {
			offGC.setColor(Color.black);
			offGC.drawLine(xorg+wh-1, yorg+hb, xorg+wh-1, yorg+height-1);
			offGC.drawLine(xorg+wh+1, yorg+hb, xorg+wh+1, yorg+height-1);
			offGC.setColor(Color.white);
			offGC.drawLine(xorg+wh, yorg+hb, xorg+wh, yorg+height-1);
		}

		if ((pipes & Symbols.PIPE_MASK[Symbols.EAST]) != 0) {
			offGC.setColor(Color.black);
			offGC.drawLine(xorg+wb, yorg+hh-1, xorg+width, yorg+hh-1);
			offGC.drawLine(xorg+wb, yorg+hh+1, xorg+width, yorg+hh+1);
			offGC.setColor(Color.white);
			offGC.drawLine(xorg+wb, yorg+hh, xorg+width, yorg+hh);
		}

		if ((pipes & Symbols.PIPE_MASK[Symbols.WEST]) != 0) {
			offGC.setColor(Color.black);
			offGC.drawLine(xorg+1, yorg+hh-1, xorg+wa, yorg+hh-1);
			offGC.drawLine(xorg+1, yorg+hh+1, xorg+wa, yorg+hh+1);
			offGC.setColor(Color.white);
			offGC.drawLine(xorg+1, yorg+hh, xorg+wa, yorg+hh);
		}
	}
}
