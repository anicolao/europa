/*
 * @(#)ClientFrame.java
 */
package games.Battle.client.ClientApplet;

import java.awt.Frame;
import java.awt.Event;

/**
 * The ClientFrame is a Frame which recognizes WINDOW_DESTROY
 * events and processes them in a way friendly to an executing
 * ClientApplet.
 *
 * @author Alex Nicolaou
 * @author Jay Steele
 */
public class ClientFrame extends Frame {

	/**
	 * The game applet this frame will be "holding".
	 */
	ClientApplet gameApplet;

	/**
	 * Construct a ClientFrame with the given title and containing 
	 * the given applet. If the frame is Closed, the applet is
	 * destroyed.
	 * @param title the title for the frame
	 * @param applet the ClientApplet to destroy on a CLOSE_WINDOW event
	 * @see ClientApplet
	 */
	public ClientFrame(String title, ClientApplet applet) {
		super(title);
		gameApplet = applet;
	}

	/**
	 * Process events from the frame.
	 * @param e an event
	 */
	public boolean handleEvent(Event e) {
		if (e.id == Event.WINDOW_DESTROY) {
			gameApplet.destroy();
			dispose();
			return true;
		}
		return super.handleEvent(e);
	}
}
