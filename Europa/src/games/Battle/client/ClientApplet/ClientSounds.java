/*
 * @(#)ClientSounds.java
 */

package games.Battle.client.ClientApplet;

import java.applet.*;
import java.awt.*;
import java.awt.image.*;
import java.net.*;
import java.util.Random;

import games.image.*;
import games.Battle.shared.sys.*;

/**
 * ClientSounds is responsible for loading and building all of the
 * client sounds.
 *
 * @author Alex Nicolaou
 * @author Jay Steele
 */

public class ClientSounds {

	static Random rand = new Random();

	/**
	 * The applet used to obtain the URL.
	 */
	public static Applet applet = null;

	/**
	 * The directory the sound files were located in.
	 */
	private static String soundDir = "sounds";

	/**
	 * The URL to load the sounds from.
	 */
	private static URL docBase = null;

	/**
	 * A flag to permit or deny sounds from playing.
	 */
	private static boolean playOn = true;

	/**
	 * The gun sound.
	 */
	public static AudioClip gun = null;

	/**
	 * The paratrooper sound.
	 */
	public static AudioClip paratrooper = null;

	/**
	 * The click sound.
	 */
	public static AudioClip click = null;

	/**
	 * The various fighting sounds.
	 */
	public static AudioClip fight[] = new AudioClip[4];

	/**
	 * A sound to play for a login
	 */
	public static AudioClip login = null;

	/**
	 * Retrieves an audio clip. This method look after ensuring the 
	 * image is loaded from the correct URL, and the pathname
	 * of the image.
	 * @param name the filename of the sound to retrieve
	 */
	private static AudioClip getAudioClip(Object applet, String name) {
	    //AudioClip clip = applet.getAudioClip(docBase, soundDir+"/"+name);
	    AudioClip clip = null;

	    URL audioURL = applet.getClass().getResource("/sounds/" + name);
	    try {
		clip = Applet.newAudioClip(audioURL);
	    } 
	    catch (Throwable t) {
		try {
		    clip = ((Applet)applet).getAudioClip(docBase, soundDir + "/" + name);
		}
		catch (Throwable tt) {
		    System.out.println("couldn't load audio clip " + name);
		}
	    }

	    return clip;
	}

	/**
	 * Get the images. The method blocks until all of the images
	 * have been loaded.
	 * @param a the applet for the URL 
	 */
	public static void loadAll(Applet a) {
		applet = a;
		try { 
			docBase = a.getDocumentBase();
			if (docBase == null)
				docBase = new URL("file:/C:/projects/Java/Battle/client/battle.html");
		}
		catch (Exception e) {
			try {
				docBase = new URL("file:/C:/projects/Java/Battle/client/battle.html");
			}
			catch (Exception malformed) {
				System.out.println("malformed url");
			}
		}

		try {
		    gun 		= getAudioClip(a, "gun.au");
		    paratrooper = getAudioClip(a, "paratrooper.au");
		    click 		= getAudioClip(a, "click.au");
		    login 		= getAudioClip(a, "login.au");
		    for (int i=0; i<fight.length; i++) {
			    fight[i]	= getAudioClip(a, "fight"+i+".au");
		    }
		}
		catch (NullPointerException e) {
		    System.out.println("Failed to get audio. Sound off.");
		}
	}

	/**
	 * Play a click sound, if sound play is currently on.
	 */
	public static synchronized void playClick() {
		if (playOn && click != null) click.play();
	}

	/**
	 * Play a login sound, if sound play is currently on.
	 */
	public static synchronized void playLogin() {
		if (playOn && login != null) login.play();
	}

	/**
	 * Play a gun sound, if sound play is currently on.
	 */
	public static synchronized void playGun() {
		if (playOn && gun != null) gun.play();
	}

	/**
	 * Play a paratrooper sound, if sound play is currently on.
	 */
	public static synchronized void playParatrooper() {
		if (playOn && paratrooper != null) paratrooper.play();
	}

	/*
	 * Randomly play one of the fighting sounds, if sound play
	 * is currently on.
	 */
	public static synchronized void playRandomFight() {
		if (playOn) {
			int idx = Math.abs(rand.nextInt()) % fight.length;
			if (fight[idx] != null)
				fight[idx].play();
		}
	}

	/**
	 * For those who don't like sounds, they can turn them off.
	 */
	public static synchronized void disableSounds() { playOn = false; }

	/**
	 * For those who like sounds, they can turn them on.
	 */
	public static synchronized void enableSounds() { playOn = true; }
}
