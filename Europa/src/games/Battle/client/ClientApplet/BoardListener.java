package games.Battle.client.ClientApplet;

import java.io.OutputStream;

public interface BoardListener {
    public void setTurn(byte tn);
    public void setPlayer(int p);
    public void setOutputStream(OutputStream os);
    public void boardChanged(ClientBoard board);
}
