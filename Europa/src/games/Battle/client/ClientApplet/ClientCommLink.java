/*
 * @(#)ClientApplet.java
 */

package games.Battle.client.ClientApplet;

import java.awt.*;

/**
 * This class is a simple animation thread which displays a 
 * "please wait" animated graphic while the client and server
 * establish communication with each other at the beginning of 
 * a game.
 *
 * The ClientCommLink execution has absolutely no mercy with
 * repect to whatever currently exists in the graphic display
 * of the component it is contructed with. When started, the
 * runnable will simply continually write over the entire client
 * area.
 *
 * @author Alex Nicolaou
 * @author Jay Steele
 */
public class ClientCommLink implements Runnable {
	Component comp;

	static String[] msg = { "Please wait as a subspace commlink is",
							"established with orbiting satellite..." };

	/** 
	 * Construct a ClientCommLink class, using the given component
	 * to draw itself in.
	 */
	public ClientCommLink(Component c) {
		comp = c;
	}

	/**
	 * The run method for the ClientCommLink instance, executed when
	 * a controlling thread is started. This method simply loops 
	 * forever (until it is killed) display its contents and
	 * doing its animation.
	 */
	public void run() {
		Graphics g = comp.getGraphics();
		Rectangle b = comp.bounds();

		// g.setFont( ? );
		
		FontMetrics fm = g.getFontMetrics();

		// Determine the height of the font, and the total height
		// of the message
		int fh = fm.getHeight();
		int h = fh * msg.length;

		// determine the widths of each individual message
		int[] sw = new int[msg.length];
		for (int i=0; i<msg.length; i++) {
			sw[i] = fm.stringWidth(msg[i]);
		}

		int count = 0;
		while (true) {

			double cx = (double)b.width / 2.0;
			double cy = (double)b.height / 2.0;

			// clear the background
			g.setColor(Color.black);
			g.fillRect(0, 0, b.width, b.height);

			// write the text, centered in the component
			g.setColor(Color.white);
			int x, y;
			int starty = (int)(cy - (double)fh/2.0);
			for (int i=0; i<msg.length; i++) {
				x = (int)(cx - (double)sw[i]/2.0);
				y = starty + i * fh;

				g.drawString(msg[i], x, y);
			}

			int lightDist = 200;
			int numLights = 10;
			int lightDelta = (int)((double)lightDist / (double)numLights);
			int startLightX = (int)(cx - (double)lightDist / 2.0);
			int startLightY = (int)(starty + h + 20);

			g.setColor(new Color(255, 0, 25*count));
			g.fillOval(startLightX+(count*lightDelta), startLightY,
							10, 10);

			count = count + 1;
			count = count % numLights;

			try {
				Thread.currentThread().sleep(100);
			} catch (Exception e) {}
		}
	}
}
