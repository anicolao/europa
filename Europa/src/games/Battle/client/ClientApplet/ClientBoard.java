/*
 * @(#)ClientBoard.java
 */

package games.Battle.client.ClientApplet;

import java.awt.*;
import games.Battle.shared.sys.*;

/**
 * The ClientBoard is the subclass of the Board class which is
 * responsible for maintaining the client version of the game 
 * board. Its fundamental purpose is to create the appropriate
 * ClientCell in its makeCell method.
 *
 * @author Alex Nicolaou
 * @author Jay Steele
 */
public class ClientBoard extends Board
{
	/**
	 * Construct a client game board.
	 */
	public ClientBoard() {
		super();
	}

	/**
	 * Creates a ClientCell cell for the client game board.
	 * @param r the row the cell is being created for
	 * @param c the column the cell is being created for
	 * @see ClientCell
	 */
	public Cell makeCell(int r, int c) {
		return (new ClientCell(r, c));
	}

	/**
	 * Debugging method
	 */
	public void asciiDump() {
		for (int r=0; r< Rules.rows; r++) {
			for (int c=0; c< Rules.cols; c++) {
				ClientCell cell = (ClientCell)getCell(r,c);
				switch (cell.getOccupancy()) {
					case Symbols.PLAYER0:
						System.out.print("0 ");
						break;
					case Symbols.PLAYER1:
						System.out.print("1 ");
						break;
					case Symbols.PLAYER2:
						System.out.print("2 ");
						break;
					case Symbols.PLAYER3:
						System.out.print("3 ");
						break;
					case Symbols.PLAYER4:
						System.out.print("4 ");
						break;
					case Symbols.UNOCCUPIED:
						System.out.print(". ");
						break;
					case Symbols.INVISIBLE:
						System.out.print("X ");
						break;
					case Symbols.UNMODIFIED:
						System.out.print("U ");
						break;
					default:
						System.out.print("? ");
						break;
				}
			}
			System.out.println();
		}
	}
}
