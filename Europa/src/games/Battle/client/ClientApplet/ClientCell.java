/*
 * @(#)ClientCell.java
 */

package games.Battle.client.ClientApplet;

import java.awt.*;

import games.Battle.shared.sys.*;

/**
 * A cell which is placed inside a ClientBoard. It is responsible for
 * maintaining client-speficic data for the game cell.
 *
 * @author Alex Nicolaou
 * @author Jay Steele
 */
public class ClientCell extends Cell
{
	/**
	 * The cached width and height of a board cell
	 */
	int width, height;

	/**
	 * Build a client cell, which is located at the given (r,c) pair
	 * @param r the cell's row
	 * @param c the cell's column
	 */
	public ClientCell(int r, int c) {
		super(r, c);
		width = (int)((float)Symbols.BOARD_W / (float)Rules.cols);
		height = (int)((float)Symbols.BOARD_H / (float)Rules.rows);
	}

	/**
	 * Return which pipe region the (x,y) tuple is located in. Returns
	 * the one of Symbols.NORTH, Symbols.EAST, Symbols.WEST, 
	 * Symbols.SOUTH, or -1 if the pair does not fall inside the
	 * region of any pipes.
	 * @param x the x-coordinate
	 * @param y the y-coordinate
	 */
	public int pipeRegion(int x, int y) {
		float x0 = (float)0.0;
		float x1 = (float)width / (float)3;
		float x2 = x1 * (float)2;
		float x3 = width;
		float y0 = (float)0.0;
		float y1 = (float)height / (float)3;
		float y2 = y1 * (float)2;
		float y3 = height;

		if (x > x1 && x < x2) {
			if (y > y0 && y < y1) {
				return Symbols.NORTH;
			} else if (y > y2 && y < y3) {
				return Symbols.SOUTH;
			}
		}

		if (y > y1 && y < y2) {
			if (x > x0 && x < x1) {
				return Symbols.WEST;
			} else if (x > x2 && x < x3) {
				return Symbols.EAST;
			}
		}

		return -1;
	}
}
