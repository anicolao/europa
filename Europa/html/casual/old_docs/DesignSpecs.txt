The Java Battle Client-Server Model Specs
-----------------------------------------

Introduction:

The Java Battle game operates via several
running processes, possibly distributed
over a wide area network like the internet.
The game is organized and referee'd by
a server. Human players interact with a 
game by connecting with clients. The clients
are responsible for taking the information
given to it by the server and displaying
it, and also for receiving input from the
player and transmitting it to the server.

Communication:

The communication between the client and
server is bidirectional. The server sends
information such as board state updates
to the client. The client sends information
such as vector addition or removal requests
to the server.

We are going to make a number of assumptions
in order to come up with a reasonable network
model which will allow the game to be played
over a network in which some clients are not
guaranteed a large amount of bandwidth.

Our model will assume that a client can
receive information at a rate of 1000 bytes per
second. We will also assume that we are 
interested in executing 2 "turns" every 
second. In other words, in an ideal world,
the entire board will be updated on the client
2 times every second.

This means that we can transmit 512 bytes maximum
to an individual client in one turn. This upper
bound will allow us to determine how large the
playing field is and how much information we
can associate with each cell in the field.

If we assume that the board terrain is not 
going to change during the course of a game,
and that cities can not be created and 
destroyed, we need to transmit the following
information about each cell during a transmission:

* the cell's location (row x col)
* the player inhabiting the cell (or empty)
* the number of troops in the cell
* the state of the vectors in the cell

Assume a board size of 16 x 16. Then we have
the location fitting into 8 bits (4+4).
