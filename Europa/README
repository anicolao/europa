Europa README
-------------

Contents

0. Overview
1. Running the Server
2. Running the Client
3. Compiling the Source
4. Tested Platforms
5. Running without Symbolic Links
6. More Documentation
7. Feedback

-----------------------------------------------------------------------

0. Overview

The license for this software is in a separate file called "LICENSE".
Please read it first.

This README contains directions for setting up and running Europa.
Europa is a multi-player client-server game, so the instructions for
running the server must be carried out first. These instructions are
followed by a list of platforms that the game has been tested on.

At the moment the distribution is very UNIX-centric in that it assumes
that you really want to set up the server on a Solaris machine. It is
possible to set up and run on an NT machine but you'll find that not
all the scripts are there, and that you'll require a Korn Shell
implementation that you may not have.

The instructions assume the existence of a Korn Shell and some other
standard UNIX-style tools. We used the MKS toolkit to test under
Windows, and recommend it. If you don't have such tools you will have
to read and understand the scripts to carry out what they do by hand.
We have also assumed the existence of correctly installed TCP/IP network
support.

We compiled and tested using the Java Development Kit v1.0.2. We've made
an effort to test on a variety of platforms, but the main platform used
for testing the server was Solaris 2.5.1.

Directories:

README	- this file
bin		- handy scripts for running the client & server
classes - actual .class files
html	- WWW pages for the game, with rules & applet
server	- directory to run the server, with images/sounds/player file/logs
src		- source tree for the distribution


1. Running the Server

The server must be started before the game can be played. You need to
have java set up so that you can run applications using the java
interpreter directly, and have Korn Shell installed in /bin/ksh for
things to work without modification. On Windows 95, or Windows NT, we
tested the distribution using the MKS Toolkit Korn Shell. The scripts
for PCs have .ksh extensions and the UNIX scripts have no extension.

Test your java installation by running a java applet out of the demo
directory. For example, cd to java/demo/ArcTest and issue the command
java ArcTest. If the ArcTest applet comes up the scripts for running
the client and server should work without modification.

The first step is to make sure that the server is compiled. You'll need
to be sure that you are set up to do java stuff: if you can compile and
run some of the demo programs in the JDK then you should be fine. To 
compile Europa, go to Europa/bin and run the unpack script.

If you already have an older player database, copy it into the server 
directory and name it players.dat. Otherwise, copy the sample file into
players.dat. The sample is named players.dat.sample and is already in the
server directory.

To load the server, cd to the Europa/bin directory, and execute the
startserver script. This will start the server, and the server log
file will be placed in Europa/server/log. You can tail -f log to
watch the log messages as they are entered into the server; a typical
correct startup will look like:

Wed Mar 27 04:32:41 EST 1996: log service started
Wed Mar 27 04:32:41 EST 1996: finished reading player file
Wed Mar 27 04:32:41 EST 1996: Starting server on port #5000
Wed Mar 27 04:32:41 EST 1996: Max players = 128

After 10 minutes the player file will be auto-saved, which will be the
next log message if nobody logs in in that time. This script is simple
and extensively tested. If you have a problem, make sure you can run
ArcTest, and make sure you are running the script in Europa/bin.

Once the server is running, you are ready to load the client.

2. Running the Client

To begin with, test that everything is working by running the client on
the same machine as the server. This can be done by running the europa
script in the Europa/bin directory. It will start an applet viewer, and
a login dialog. You can click "I'm new" to create a new player and the
game selection screen will appear. From this screen just choose "Quit"
to quit - the client is working and it is time to set it up from the
web pages. Alternately, you can log in with the built in admin account,
named admin, password admin. Naturally you should change this password
using the "Change Info" button after you have logged on.

Create a symbolic link from your web pages to the Europa/html/Europa
directory.  This will allow people to view the Europa pages to read the
rules and play the game. Do not create a symbolic link to any directory
above the Europa/server directory because the player file, complete
with non-encrypted passwords, could be compromised if you do that
(encryption is forbidden for contest entries; we'll add typical UNIX
crypt encryption for a later release). Once the symlink is made you can
load the pages into your java-capable WWW browser. Alternatively, you
can use the browser's file..open menu (if it has it) to open the html
files directly.

Take a moment to browse the pages to be familiar with the game since it
is quite complex and not necessarily intuitive if you do not know the
controls. Then take the plunge, and load the log-in and play. Because the
game runs in separate windows, you can always keep the controls instructions
up while playing to refresh your memory at first.


3. Compiling the Source

To rebuild the entire source hierarchy, cd Europa/src and run ./buildall
which will recompile the entire client and server. If you have tested
running the script to start the server then the buildall script should
run without modification. The source should build without any errors.

To compile an individual directory or file, use the -d option to the
javac compiler to ensure that the .class files are placed in the
Europa/classes directory. Also when compiling you need to ensure that
the Europa/classes directory is in the CLASSPATH; this is done 
automatically by buildall but must be done by hand to compile an
individual directory or file.

4. Tested Platforms

We have made an effort to test the client and server under a variety of
platforms. The server has been run on SunOS 5.5, Windows 95, and Windows
NT 3.51. The client works on SunOS 5.5, IRIX 5.4, Linux (version unknown),
Windows NT 3.51, and Windows 95. The game is designed to work well with
even slow network connections; it is built around the assumption of that
players will connect via PPP or SLIP over a slow modem link. We have tested
under these conditions at length, perhaps because we're addicted.

5. Running without Symbolic Links

Under Windows NT and Windows 95 you will need to copy the classes/games
directory and the classes/win directory to the html/Europa/client
directory. Under UNIX it is more efficient to leave the existing symbolic
links. If you actually plan to modify any part of the client under NT,
it is best to eliminate one of the classes directories and only use
the other one to avoid confusion as to what classes are being loaded.

Also, in the Europa/html/Europa directory, copy Europa.html to index.html.

6. More Documentation

All of the classes have been prepared for processing by javadoc, the java
utility for producing documentation from .java files. The resulting documents
are in Europa/docs and provide a convenient reference to the classes. In
addition, there is a small amount of casual documentation, which documents
the thinking and development process behind the game, in Europa/html/casual.
These pages aren't linked in off of the main WWW pages in Europa/html/Europa
because they aren't intended for end user consumption.

7. Feedback

The authors proud of this product and would welcome feedback that you may
have, both positive and negative. You can reach us for help, to request new
features, or just to say you like it/hate it, via our e-mail addresses. The
addresses are on our home pages, which are referred to right off of the 
Europa pages. We hope you enjoy the game! 
